<ul id="children-page">

			<?php 
			
			if(isset($_SESSION['usuario']) && !empty($_SESSION['usuario']["nome"])) {
				//$pages = array(7020,89,7027,95);
				$pages = array(7020,89,95);
			}
			else{
				$pages = array(86,89,95);
			}
			
			global $post;
			foreach($pages as $key => $page) {
				
				$active  = ($page == $post->ID) ? 'active' : '';
				
				printf('<li class="%s"><a href="%s">%s</a></li>',$active,get_permalink($page),get_the_title($page));
				
			}

			?>
</ul>