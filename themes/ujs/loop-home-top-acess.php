<div class="loop-home-top-acess row-fluid">
<?php

	$posts = get_posts_top_acess(3);
	
	foreach($posts as $key => $post) {

		$post = get_post($post->post_id); 
?>
		
		<div class="item span4">
		
			<div class="content">
				<h2 class="title"  style="height:60px;"><a href="<?php echo get_permalink($post->ID) ?>"><?php echo $post->post_title?></a></h2>
				<div class="time"><?php echo get_post_time("d/m/Y",false,$post->ID)?></div>
				
                <div class="excerpt"><?php echo get_excerpt($post->post_content,200)?></div>
			</div>
		
		</div>
		
		
<?php 
		
	}

?>

</div>
