<?php
/*
 * Template name: Direção
 */


global $post,$paged;


get_header();

have_posts(); the_post();


?>

<script type="text/javascript">
jQuery(function(){
direcao_orderby();
});
</script>

<aside>

	<div class="container page-inside template-timeline template-direcao">
	
		<div class="title-inside">
			<h2 class="title-pages">SOBRE A UJS</h2>
		</div>
		
		<?php get_template_part('children','sobre')?>
		
		<h2 class="title-pages">
			<span style="width:10%;"><?php the_title()?></span>
			<div class="bg" style="width:88%"></div>
		</h2>
		
		<div class="orderby">Ordernar por: 
			<select name="orderby" id="">
				<option value="">Selecionar</option>
				<option value="nome" <?php echo ($_GET['orderby'] == "nome") ? 'selected="true"' : '';?> data-url="<?php the_permalink()?>?orderby=nome">Ordem Alfabética</option>
				<option value="funcao" <?php echo ($_GET['orderby'] == "funcao") ? 'selected="true"' : '';?> data-url="<?php the_permalink()?>?orderby=funcao">Função</option>
			</select>
		</div>
		
		<div class="row-fluid">
			
			
		
			<div class="span9">
			
				<div class="loop-direcao">
	
					<?php 
					$args = array(
							"post_type" => "direcao",
							"posts_per_page" => "21",
							"paged" => $paged,
							"orderby" => "title",
							"order" => "asc"
							);
					if(isset($_GET['orderby'])) {
						$orderby = $_GET['orderby'];
						
						if($orderby == "nome") {
							$args["orderby"] = "title";	
							$args["order"] = "ASC";
						} else if($orderby == "funcao") {
							$args['orderby'] = "meta_value";
							$args["meta_key"] = "cargo";
						} else {
							$args["orderby"] = "title";
							$args["order"] = "ASC";
						}
						
					}
					$query = new WP_Query($args);
					$i = 1;
					$a=1;
					while($query->have_posts()): $query->the_post();
					echo (1 == $i) ? '<div class="row-fluid">' : '';
					?>
					
						<div class="item span4">
							<div class="thumb" style="background:url(<?php echo get_url_thumbnail(get_the_ID(), 'full')?>)"></div>
							<div class="name-cargo">
								<p class="name"><?php the_title()?></p>
								<p class="cargo"><?php echo get_post_meta(get_the_ID(),'cargo',true)?></p>
							</div>
							<div class="dados">
								
								<ul>
									<li><i class="ico" id="email"></i><a href="mailto:<?php echo get_post_meta(get_the_ID(),'e-mail',true)?>"><?php echo get_post_meta(get_the_ID(),'e-mail',true)?></a></li>
									<li><i class="ico" id="twitter"></i><?php echo get_post_meta(get_the_ID(),'twitter',true)?></li>
									<li><i class="ico" id="face"></i><?php echo get_post_meta(get_the_ID(),'facebook',true)?></li>
								</ul>
								
							</div>
						</div>
					
					<?php 
					if($i == 3 OR $a == $query->post_count) {
						$i = 1;
						echo '</div>';
					} else {
						$i++;
					}
					$a++;
					endwhile;
					?>
				
				
				<div class="pagination"><?php pagination_funtion($query->max_num_pages)?></div>
			
			</div>
			
			</div>
			
			<div class="span3 sidebar">
				<?php dynamic_sidebar('sidebar-direcao')?>
			</div>
			
		</div>
		
		<br class="both" />	
			
		<?php get_template_part('template','tarja-filiese')?>
	
	</div>

</aside>


<?php 
get_footer();
?>