<footer>

	<div class="container">
		<div class="row-fluid">
			<?php dynamic_sidebar('sidebar-rodape')?>
			<div class="clearfix"></div>
		</div>
	</div>
	
	
	<!--RODAPE Adicionado ao footer, copiar esse conteudo no fim da tag footer(antes do fechamento)-->
		<div class="clearfix"></div>
		<div class="rodape">
			<div class="container">
				<div class="row-fluid">
					<div class="span2">
						<ul>
							<li class="principal"><a href="/filie-se/">FILIE-SE À UJS</a></li>
							<li class="hidden-phone"><a href="/filie-se/">FILIE-SE</a></li>
							<li class="hidden-phone"><a href="/filie-se/filie-se-a-ujs/">CARTEIRA NACIONAL<br/>DO MILITANTE</a></li>
							<li class="hidden-phone"><a href="/contribua-com-a-ujs-2/">CONTRIBUA COM<br/>A UJS</a></li>
						</ul>
					</div>
					<div class="span2">
						<ul>
							<li class="principal"><a href="/sobre-a-ujs/nossa-historia/">SOBRE A UJS</a></li>
							<li class="hidden-phone"><a href="/sobre-a-ujs/nossa-historia/">NOSSA HISTÓRIA</a></li>
							<li class="hidden-phone"><a href="/categoria-direcao/executiva-nacional/">DIREÇÃO</a></li>
							<li class="hidden-phone"><a href="/sobre-a-ujs/estatuto/">ESTATUTO</a></li>
							<li class="hidden-phone"><a href="/sobre-a-ujs/ujs-nos-estados/">UJS NOS ESTADOS</a></li>
						</ul>
					</div>
					<div class="span2">
						<ul>
							<li class="principal"><a href="/downloads/">DOWNLOADS</a></li>
							<li class="hidden-phone"><a href="/material-download/17o-congresso-da-ujs/">CONGRESSOS</a></li>
							<li class="hidden-phone"><a href="/material-download/outros/">OUTROS</a></li>
						</ul>
					</div>
					<div class="span2">
						<ul>
							<li class="principal"><a href="/manifestos/">MANIFESTO</a></li>
							<li class="hidden-phone"><a href="/wp-content/uploads/2013/09/manifesto_ujs.pdf">DOWNLOAD DO<br/>MANIFESTO</a></li>
						</ul>
					</div>
					<div class="span2">
						<ul>
							<li class="principal"><a href="/home-noticias/">NOTÍCIAS</a></li>							<li class="hidden-phone"><a href="/noticias/">ÍNDICE DE NOTÍCIAS</a></li>							<li class="hidden-phone"><a href="/home-noticias/">COLUNISTAS</a></li>						</ul>					</div>					<div class="span2">						<ul>							<li class="principal"><a href="/fale-conosco/">FALE CONOSCO</a></li>						</ul>					</div>				</div>			</div>					<!--<div style="display: block; text-align: center; font-weight:normal !important;">desenvolvido e hospedado por: <a href="http://www.inventivos.com.br" style="color: #FFF" target="_blank">&lt;/&gt; inventivos</a></div>-->
		</div>
	<!--FIM DO RODAPE-->
	
	
</footer>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8380382-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<?php
wp_footer();