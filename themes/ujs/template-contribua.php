<?php
/*
 * Template name: Contribua com a UJS
 */


global $post;


get_header();

have_posts(); the_post();


?>

<script type="text/javascript">
jQuery(function(){

});
</script>

<aside>

	<div class="container page-inside template-filie-se template-contribua">
	
	<div class="title-inside">
		<h2 class="title-pages">FILIE-SE À UJS</h2>
	</div>
	
	<?php get_template_part('children','filiese')?>
	
	<h2 class="title-pages">
		<span style="width:30%;"><?php the_title()?></span>
		<div class="bg" style="width:64%"></div>
	</h2>
			
	<div class="content-post" style="margin-left:10%; display:inline-block;">
	
	<?php the_content()?>
	
		
	
	</div>

</aside>

<?php 
get_footer();
?>