<?php
/*
 * 
 * Template name: Mais lidas
 * 
 */
get_header();



?>

<aside>

	<div class="container single-page template-noticias">
		
		<div class="title-inside">
			<h2 class="title-pages">Notícias</h2>
		</div>
		
		
		<div class="content-padding">

			<div class="row-fluid">
				
				<div class="span9">
				
				<h2 class="title-pages">
					<span style="width:25%;">Notícias mais lidas</span>
					<div class="bg" style="width:64%"></div>
				</h2>
				
				<?php 
				global $post_type;
				$the_post_type = get_post_type_object($post_type);
	// 			print_r($the_post_type);
				?>
	
				<div class="loop-noticias">
					<?php
					global $paged; 
					$posts = get_posts_top_acess(50);
					foreach($posts as $key => $post) {
					
						$post = get_post($post->post_id);
					?>
						
						<div class="item">
							<time><?php the_time('d/m/Y')?></time>
							<h3 class="title"><a href="<?php the_permalink()?>"><?php the_title();?></a></h3>
							<div class="excerpt"><?php echo get_excerpt($post->post_content,100)?></div>
						</div>
					
					<?php 
					};
					?>
				</div>
				
				<?php get_template_part('paginacao')?>
				
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>