<?php
get_header()
?>

<aside>

	<div class="container single-page">
		
		<div class="content-padding">
		
			<div class="row-fluid">
				
				<div class="span9">
				
				<?php if(have_posts()): the_post()?>
					
					<div class="row-fluid">
						
						<div class="span1 time">
						
						</div>
						
						<div class="span11 content-post">
							<h2 class="title"><?php the_title()?></h2>
							
							<?php 
							if(has_post_thumbnail()) {
							?>
								<p><?php the_post_thumbnail('full');?></p>
							<?php 
							}
							?>

							<?php the_content()?>
							
							<p id="author-category">
								Por <a href="<?php echo bloginfo('url')."/".the_author()?>"><?php the_author()?></a> em <?php echo the_terms(get_the_ID(), 'categoria-noticia','' ,', ')?>
							</p>
							
							<p id="tags">
								Tags: <?php the_terms(get_the_ID(), 'tags', '', ', ')?>
							</p>
							
						</div>
					
					</div>
				
				<?php endif;?>
				
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>