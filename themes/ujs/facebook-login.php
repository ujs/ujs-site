<?php
ini_set('display_errors', 1);
error_reporting(E_ALL^E_NOTICE^E_WARNING);
require_once 'facebook-php-sdk-master/src/facebook.php';

// Credentials
$fb_app_id = '1429184583964098';
$fb_app_secret = '5392dae8d909c946ec52e37d9fac1c92';

$facebook = new Facebook(array('appId'=>$fb_app_id, 'secret'=>$fb_app_secret));

Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;

// Get User ID
$user = $facebook->getUser();

if ($user) {
	try {
		// Proceed knowing you have a logged in user who's authenticated.
		$user_profile = $facebook->api('/me');
		
		$_SESSION['email_fb'] = $user_profile['email'];
		echo $user_profile['email'];
		Header("Location: {$_GET['url']}");
		
	} catch (FacebookApiException $e) {
		error_log($e);
		$user = null;
	}
} else {
	$loginUrl = $facebook->getLoginUrl();
	Header("Location: {$loginUrl}");
}

// // Permissions
// $allow = 'email,user_status';

// $loginurl = $facebook->getLoginUrl(array('scope'=>$allow, 'display'=>'popup'));

// $get_fb_user = $facebook->getUser();

// // Offline
// if (is_null($get_fb_user) or $get_fb_user == 0) {

// 	header("Location: {$loginurl} ");
// 	exit;

// 	// Online
// } else {

// 	try {

// 		// Verify
// 		$facebook->api($get_fb_user);

// 		// Token
// 		$service_token = $facebook->getAccessToken();

// 		// User Details
// 		$fql = "SELECT email FROM user WHERE uid = me()";
// 		$user = $facebook->api(array('method'=>'fql.query', 'query'=>$fql));

// 		echo '
// 		<ul>

// 		<li>User email: '.$user['0']['email'].'</li>

// 		</ul>';

// 	} catch (FacebookApiException $e) {

// 		header("Location: {$loginurl} ");
// 		exit;
// 	}

// }