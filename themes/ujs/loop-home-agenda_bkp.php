<?php
$query = new WP_Query(array(
			"post_type" => "agenda",
			"posts_per_page" => 9,
			"meta_key" => "data",
			"orderby" => "meta_value",
			"order" => "ASC"
		));
 
while($query->have_posts()):  $query->the_post();


$dates[] = $post;

endwhile;
?>

<div class="row-fluid loop-home-agenda">

	<?php 
		$a = 1;
		$b = 1;
		for($i = 0; $i < count($dates); $i++) {
			$post = $dates[$i];
			$date = get_post_meta($post->ID,'data',true);
			echo ($a == 1) ? '<div class="span4">' : '';
	?>
	
				<div class="item">
					<div class="date">
						<?php echo date("d",strtotime($date))?>
					</div>
					<div class="excerpt">
						<a href="<?php echo get_permalink($post->ID)?>"><?php echo get_the_title($post->ID)?></a>
					</div>
				</div>
	
		
	<?php 	
			echo ($a == 3 OR $b == count($dates)) ? '</div>' : '';
			
			if($a == 3) {
				$a = 1;
			} else {
				$a++;
			}
			$b++;
			
		}
	?>

</div>