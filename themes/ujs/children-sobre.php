<ul id="children-page">

			<?php 
			$pages = array(102,104,106,108);
			global $post;
			foreach($pages as $key => $page) {
				
				$active  = ($page == $post->ID) ? 'active' : '';
				
				if(104 != $page) {
				printf('<li class="%s"><a href="%s">%s</a></li>',$active,get_permalink($page),get_the_title($page));
				} else {
				printf('<li class="%s"><a href="%s">%s</a></li>',$active,get_term_link('executiva-nacional','categoria-direcao'),get_the_title($page));
				}
				
			}

			?>
</ul>