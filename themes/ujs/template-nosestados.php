<?php
/*
 * Template name: UJS Nos estados
 */

get_header();

?>

<script type="text/javascript">
jQuery(function(){

});
</script>

<aside>

	<div class="container page-inside template-timeline template-estatuto template-nosestados">
	
	<div class="title-inside">
		<h2 class="title-pages">SOBRE A UJS</h2>
	</div>
	
	<?php get_template_part('children','sobre')?>
	
	<h2 class="title-pages">
		<span style="width:20%;">ujs nos estados</span>
		<div class="bg" style="width:78%"></div>
	</h2>
	
	<div class="content">
	
	<?php 
		$query = new WP_Query(array("post_type" => "ujsnosestados", "orderby" => "title", "order" => "ASC", "posts_per_page" => "-1"));
		global $wp_query;
		
		$i = 0;
		$left = array();
		$right = array();
	
		while($query->have_posts()): $query->the_post();
		
		$div = (int)($query->post_count/2);

		if($i < $div) {
			$left[] = array(get_the_title(),
							get_post_meta(get_the_ID(),'site',true),
							get_post_meta(get_the_ID(),'fanpage',true)
					);
		} else {
			$right[] = array(get_the_title(),
					get_post_meta(get_the_ID(),'site',true),
					get_post_meta(get_the_ID(),'fanpage',true)
			);
		}
		
		$i++;
		endwhile;
		

	?>
	
		<div class="loop row-fluid">
		
			<div class="span6 left">
				<?php 
					$count = count($left);
					for($i=0;$i<$count;$i++) {
				?>
				
					<div class="item">
						<h3><?php echo $left[$i][0]?></h3>
						<p><a target="_blank" href="<?php echo $left[$i][1]?>"><?php echo $left[$i][1]?></a></p>
						<p><a target="_blank" href="<?php echo $left[$i][2]?>"><?php echo $left[$i][2]?></a></p>
					</div>
				
				<?php 
					}
				?>
			</div>
			
			<div class="span6 right">
				<?php 
					$count = count($right);
					for($i=0;$i<$count;$i++) {
				?>
				
					<div class="item">
						<h3><?php echo $right[$i][0]?></h3>
						<p><a target="_blank" href="<?php echo $right[$i][1]?>"><?php echo $right[$i][1]?></a></p>
						<p><a target="_blank" href="<?php echo $right[$i][2]?>"><?php echo $right[$i][2]?></a></p>
					</div>
				
				<?php 
					}
				?>
			
			</div>
		
		</div>
	
	</div>
	
	
	<br class="both" />	
			
		<?php get_template_part('template','tarja-filiese')?>
	
	</div>

</aside>


<?php 
get_footer();
?>