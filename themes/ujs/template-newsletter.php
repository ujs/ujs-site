<?php
/*
 * Template name: Newsletter
 */
get_header();

have_posts(); the_post();
?>

<aside>

	<div class="container page-inside template-newsletter">
	
	<div class="title-inside">
		<h2 class="title-pages"><?php the_title()?></h2>
	</div>
				
	<div class="content-post" align="center">
	
	<?php the_content()?>

	
	</div>

</aside>

<?php 
get_footer();
?>