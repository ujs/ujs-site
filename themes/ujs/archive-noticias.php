<?php
/*
 * 
 * Template name: Notícias
 * 
 */

global $post_type,$paged,$posts_per_page;

get_header();



?>

<script>
jQuery(function(){
	posts_per_page();
});
</script>

<aside>

	<div class="container single-page template-noticias">
		
		<div class="title-inside">
			<h2 class="title-pages">Notícias</h2>
		</div>
		
		
		<div class="content-padding">

			<div class="row-fluid">
				
				<div class="span9">
				
				<?php 
// 				$query_count = new WP_Query(array("post_type" => "noticias", "posts_per_page" => "-1"));
// 				wp_reset_query();
// 				$posts_per_page = (isset($_GET['ppp'])) ? $_GET['ppp'] : 10;
				$init = (int)($paged * $posts_per_page) - $posts_per_page;
// 				$query = new WP_Query(array("post_type" => "noticias", "paged" => $paged, "posts_per_page" => $posts_per_page));
				
				?>
				<?php if(!is_date()) {?>
				<div id="ppp">
					<p>
						Notícias <?php echo ($init == 0) ? '1' : $init; echo " - ".($init + $posts_per_page)?>
						de <?php echo $query_count->post_count?>
						| Itens por página 
						<select name="ppp" id="">
							<option <?php if($_GET['ppp'] == "10") echo 'selected'; ?>value="<?php echo get_post_type_archive_link($post_type)?>?ppp=10">10</option>
							<option <?php if($_GET['ppp'] == "20") echo 'selected'; ?>value="<?php echo get_post_type_archive_link($post_type)?>?ppp=20">20</option>
							<option <?php if($_GET['ppp'] == 50) echo 'selected';  ?>value="<?php echo get_post_type_archive_link($post_type)?>?ppp=50">50</option>
							<option <?php if($_GET['ppp'] == "100") echo 'selected';  ?>value="<?php echo get_post_type_archive_link($post_type)?>?ppp=100">100</option>
						</select>
						
					</p>
				</div>
				<?php }?>
				
				<h2 class="title-pages">
					<span style="width:25%;">Índice de notícias</span>
					<div class="bg" style="width:64%"></div>
				</h2>

				<div class="loop-noticias">
					<?php 
					while(have_posts()): the_post();
					?>
						
						<div class="item">
							<time><?php the_time('d/m/Y')?></time>
							<h3 class="title"><a href="<?php the_permalink()?>"><?php the_title();?></a></h3>
							<div class="excerpt"><?php echo get_excerpt(get_the_content(),100)?></div>
						</div>
					
					<?php 
					endwhile;
					?>
				</div>
				
				<?php get_template_part('paginacao')?>
				
				<?php comments_template()?>
				
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>