<?php
/*
 * 
 * Template name: As mais lidas
 * 
 */
get_header();



?>

<script>
jQuery(function(){
	posts_per_page();
});
</script>

<aside>

	<div class="container single-page template-noticias">
		
		<div class="title-inside">
			<h2 class="title-pages">Notícias</h2>
		</div>
		
		
		<div class="content-padding">

			<div class="row-fluid">
				
				<div class="span9">
				
				<?php 
				global $paged;
				$posts = get_posts_top_acess('10000000000000');
				foreach($posts as $key => $_post) {
					$_posts[] = $_post->post_id;
				}
				
	
				
					$query = new WP_Query(array("post_type" => "noticias","post__in" => $_posts,"paged" => $paged));
				?>
				<div id="ppp">
					<p>
						Notícias <?php echo ($init == 0) ? '1' : $init; echo " - ".($init + $posts_per_page)?>
						de <?php echo $query_count->post_count?>
						| Itens por página 
						<select name="ppp" id="">
							<option value="<?php the_permalink()?>?ppp=10">10</option>
							<option value="<?php the_permalink()?>?ppp=20">20</option>
							<option value="<?php the_permalink()?>?ppp=50">50</option>
							<option value="<?php the_permalink()?>?ppp=100">100</option>
						</select>
						
					</p>
				</div>
				
				<h2 class="title-pages">
					<span style="width:25%;">Indice de notícias</span>
					<div class="bg" style="width:64%"></div>
				</h2>

				<div class="loop-noticias">
					<?php 
					while($query->have_posts()): $query->the_post();
					?>
						
						<div class="item">
							<time><?php the_time('d/m/Y')?></time>
							<h3 class="title"><a href="<?php the_permalink()?>"><?php the_title();?></a></h3>
							<div class="excerpt"><?php echo get_excerpt(get_the_content(),100)?></div>
						</div>
					
					<?php 
					endwhile;
					?>
				</div>
				
				<?php get_template_part('paginacao')?>
				
				<?php comments_template()?>
				
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>