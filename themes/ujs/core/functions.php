<?php
// Function get assets css/img/js
function get_assets($type,$file) {
	
	return get_template_directory_uri()."/assets/$type/$file";
}

// Function get url the post thumbnail
function get_url_thumbnail($post_id,$size) {

	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), $size );
	return $thumb[0];

}

/** Pagination */
function pagination_funtion($max_num_pages = null) {
	// Get total number of pages
	global $wp_query;
	$total = (is_null($max_num_pages)) ? $wp_query->max_num_pages : $max_num_pages;
	// Only paginate if we have more than one page
	if ( $total > 1 )  {
		// Get the current page
		if ( !$current_page = get_query_var('paged') )
			$current_page = 1;
		 
		$big = 999999999;
		// Structure of "format" depends on whether we’re using pretty permalinks
		$permalink_structure = get_option('permalink_structure');
		$format = empty( $permalink_structure ) ? '&page=%#%' : 'page/%#%/';
		echo paginate_links(array(
				'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
				'format' => $format,
				'current' => $current_page,
				'total' => $total,
				'mid_size' => 5,
				'type' => 'list'
		));
	}
}


// Get share post
function get_share_post() {
	

	return '<div class="addthis_toolbox addthis_default_style ">
	<a class="addthis_button_preferred_1"></a>
	<a class="addthis_button_preferred_2"></a>
	<a class="addthis_button_preferred_3"></a>
	<a class="addthis_button_preferred_4"></a>
	<a class="addthis_button_compact"></a>
	<a class="addthis_counter addthis_bubble_style"></a>
	</div>

		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f382288331a4f5c"></script>';
		
}


// Get excerpt 
function get_excerpt($excerpt,$charlength = 500) {

	$charlength++;
	
	$excerpt = strip_tags($excerpt);
	
	
	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$return .=  mb_substr( $subex, 0, $excut );
		} else {
		 	$return .= $subex;
		}
		$return .= '[...]';
	} else {
		$return .= $excerpt;
	}
	
	return $return;
}




// Register nav menu
register_nav_menu('main', 'Main Menu');


// ADD THEME SUPPORT
add_theme_support( 'post-thumbnails' );
add_filter('widget_text', 'do_shortcode');


// REGISTER SIDEBAR

register_sidebar(array(
		'name' => __( 'Sidebar' ),
		'id' => 'sidebar-default',
		'before_widget' => '<div class="widget-default %2$s" id="widget-%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="title-widget">',
		'after_title' => '</h2>'
));


