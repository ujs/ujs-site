<?php
// ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();
include_once( ABSPATH . '/wp-admin/includes/class-wp-list-table.php' );
class newsletter_widget extends WP_Widget {
	
	public function newsletter_widget() {
		
		parent::__construct(false, 'Newsletter');
		
	}
	
	public function form($instance) {
		
		$defaults = array(
			"titulo" => "Newsletter",
			"width" => "267",
			"frase" => "Cadastre seu e-mail para receber nossas novidades:"
		);
		
		$instance = wp_parse_args($instance,$defaults);
		
		$arr[] = array("titulo","Título");
		$arr[] = array("frase","Frase","textarea");
		$arr[] = array("success","Frase de sucesso","textarea");
		$arr[] = array("duplicate","Frase de e-mail existente","textarea");
		$arr[] = array("error","Frase de erro","textarea");
		
		$count = count($arr);
		for($i=0;$i<$count;$i++):
			
			if($arr[$i][2] == "textarea") {
				$input = sprintf('<textarea name="%1$s" style="width:180px;" rows="4" id="%3$s">%2$s</textarea>',$this->get_field_name($arr[$i][0]),$instance[$arr[$i][0]],$this->get_field_id($arr[$i][0]));
			} else {
				$input = sprintf('<input type="text" name="%s" value="%s" style="width:180px;" id="%s" />',$this->get_field_name($arr[$i][0]),$instance[$arr[$i][0]],$this->get_field_id($arr[$i][0]));
			}
			
			$label = sprintf('<label for="%ss">%s</label><br />',$this->get_field_id($arr[$i][0]),$arr[$i][1].":");
			$html .= sprintf('<p>%s %s</p>',$label,$input);
	
			
		endfor;
		
		echo $html;
		
	}
	
	public function widget($args, $instance) {
		global $WP_Error, $post, $wpdb;
		
		if($_GET['action'] == "register-newsletter") {
			
			$new_email["post_type"] = "email-newsletter";
			$new_email["post_title"] = $_POST["email"];
			
			$headers = sprintf('From: %s <%s>' . "\r\n",get_option('bloginfo'),get_option('admin_email'));
			$message = sprintf('
						<p><strong>Houve uma nova incrição em sua Newsletter. Segue abaixo o nome e sobrenome:</strong> <br /><br />
						</p>
						<p>%s</p>
						<p>%s</p>
					',$_POST['email'], $_POST['nome']);
			
			wp_mail(get_option('admin_email'), 'Inscrição em Newsletter - '.get_option('bloginfo'), $message,$headers);
			
		}
		
		$defaults = array(
			"titulo" => "Newsletter",
			"width" => "267",
			"frase" => "Cadastre seu e-mail para receber nossas novidades:",
			"success" => "E-mail cadastrado com sucesso em nossa newsletter.",
			"duplicate" => "Este e-mail já esta cadastrado em nossa newsletter.",
			"error" => "Ocorreu um erro ao cadastrar seu e-mail em nossa newsletter."
		);
		
		$instance = wp_parse_args($instance,$defaults);
		
		$url = get_permalink()."?action=register-newsletter#form-register-newsletter";
		
		
		$html .= $args["before_widget"];
		
		$html .= $args["before_title"].$instance['titulo'].$args["after_title"];
		
		$html .= sprintf('<p class="msg">%s</p>',$instance["frase"]);
		
		$html .= '<form action="'.$url.'" method="POST" id="form-register-newsletter">';
		
		$html .= '<p><label for="email">Nome completo:</label>';
		
		$html .= '<input type="text"  name="nome" required="true"  /></p>';
		
		$html .= '<p><label for="email">E-mail:</label>';
		
		$html .= '<input type="text"  name="email" required="true"  /></p>';
		
		$style = 'width:'.$instance["width"].'px; white-space:normal; margin-bottom:10px;';
		
		if(isset($registrado)) {
			
			if($registrado) {
				$html .= sprintf('<span class="label label-success" style="%s">%s</span>',$style,$instance["success"]);
			} else {
				$html .= sprintf('<span class="label label-important" style="%s">%s</span>',$style,$instance["error"]);
			}
			
		} else if(isset($duplicate) AND $duplicate == true) {
			$html .= sprintf('<span class="label label-warning" style="%s">%s</span>',$style,$instance["duplicate"]);
		}
		
		
		
		$html .= '<button class="btn btn-danger" data-loading-text="Carregando..." >OK</button>';
		
		$html .= '</form>';
		
		$html .= $args["after_widget"];
		
		return $html;
		
	}
	
	
	
}

add_action('widgets_init', create_function('', 'return register_widget("newsletter_widget");'));

// add_action('admin_menu', "add_admin_newsletter");

function add_admin_newsletter() {
	global $_omnes;
	add_menu_page("newsletter", "Newsletter", 'manage_options', "om_newsletter",array("om_pages_newsletter","admin_newsletter"),'',19);
	add_submenu_page('om_newsletter', "Enviar e-mail", "Enviar e-mail", 'manage_options', 'om_enviar-email', array("om_pages_newsletter","enviar_email"));
}

class om_pages_newsletter {
	
	function admin_newsletter() {
		global $wpdb;
		
		echo '<div class="wrap" >
		<h2>E-mails cadastrados em sua newsletter</h2>
		';

		$tb = new table_newsletter();
		$tb->prepare_items();
		printf('<form id="email-filter" method="get">');
		printf('<input type="hidden" name="page" value="%s" />',$_REQUEST['page']);
		$tb->search_box("Pesquisar", $input_id);
		$tb->display();
		printf('</form>');
		
		echo '</div>';
	}
	
	function enviar_email() {
		
		if("enviar" === $_GET['action']) {
			
			$nremetente = $_POST['nremetente'];
			$eremetente = $_POST['eremetente'];
			
			@ignore_user_abort(TRUE);
			error_reporting(0);
			@set_time_limit(0);
			ini_set("memory_limit","-1");
			@ini_set("sendmail_from", "newsletter@liaccentralsorologica.com.br");
			@ini_set("time_limit",0);
			
			$headers = "From: $nremetente <$eremetente>\n";
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/html; charset=utf-8\n";
			$headers .= "Content-Transfer-encoding: 8bit\n";
			$headers .= "Reply-To: $nremetente <$eremetente>\n";
			$headers .= "Return-Path: $eremetente\n";
			$headers .= "Message-ID: <".md5(uniqid(time()))."@$sremetente>\n";
			$headers .= "X-Priority: 3\n";
			$headers .= "X-MSmail-Priority: High\n";
			$headers .= "X-Mailer: iGMail [www.ig.com.br]\n";
			$headers .= "X-Originating-Email: [$nremetente]\n";
			$headers .= "X-Sender: $nremetente\n";
			$headers .= "X-Originating-IP: [201.201.120.121]\n";
			$headers .= "X-iGspam-global: Unsure, spamicity=0.570081 - pe=5.74e-01 - pf=0.574081 - pg=0.574081\n";
			
			global $wpdb;
			
			$emails = $wpdb->get_results("SELECT email,ativo FROM {$wpdb->prefix}emails_newsletter WHERE ativo = '1' AND email != ''");
			
			$i=1;
			foreach($emails as $email) {
				
				$_emails[] = $email->email;
				
				$corpo = $_POST['corpo-email'];
				$corpo .= sprintf('<img src="%s" alt="" />',site_url()."/?leitura-email-newsletter=1&email=$email->email&session=".session_id());
				$corpo .= sprintf('Funcionou?');
				
				wp_mail($email->email, $_POST['assunto'], $corpo, $headers);
				
				printf('
						<script type="text/javascript">
							jQuery(function($){
								$("#qtd-emails-enviados").html("E-mail enviado para %s destinatários no total de %s");
							});
						</script>
				',$i,$wpdb->num_rows);
				
				sleep(10);
				
			$i++;
			}

			$_emails = serialize($_emails);
			
			$date = date("Y-m-d");
			
			$wpdb->insert("{$wpdb->prefix}emails_newsletter_enviados", array("emails" => $_emails, "date" => $date, "session" => session_id()));
			
			session_destroy();
			
			
			
		}
		
		echo '<div class="wrap">',
			 '<h2>Enviar newsletter</h2>';
		
		echo ($_GET['action'] == "enviar") ? '<div id="message" class="updated below-h2"><p id="qtd-emails-enviados"></p></div>' : '';
		
		printf('<form action="admin.php?page=om_enviar-email&action=enviar" method="POST">');
		
		echo '<p>
					<label for="">Nome do remetente:</label><br />
					<input type="text" name="nremetente" required="true" style="width:90%; padding:5px">
				</p>';
		
		echo '<p>
					<label for="">E-mail do remetente:</label><br />
					<input type="text" name="eremetente" required="true" style="width:90%; padding:5px">
				</p>';
		
		echo '<p>
					<label for="">Assunto:</label><br />
					<input type="text" name="assunto" required="true" style="width:90%; padding:5px">
				</p>';
		
		wp_editor('', 'corpo-email', array("textarea_name" => "corpo-email", "textarea_rows" => 5));
		
		echo '<br /><input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Enviar" accesskey="p"><br />';
		
		echo '<br /><span><strong>OBS.: O e-mail será enviado para todos os destinatários ativos na lista de assinaturas de sua newsletter</strong></span>';

		printf('</form>');
		
		echo '</div>';
		
	}
	
}

class table_newsletter extends WP_List_Table {

	function __construct(){
        global $status, $page, $_omnes;
               
        parent::__construct( array(
            'singular'  => 'e-mail', 
            'plural'    => 'e-mails',   
            'ajax'      => false        
        ) );
        
        $_omnes = new OmnesWP();
        
    }
	
	function get_columns() {
	    $posts_columns = array();
	    $posts_columns['cb'] = '<input type="checkbox" />';
	    $posts_columns['nome'] = 'Nome';
	    $posts_columns['email'] = 'E-mail';
	    $posts_columns['ativo'] = 'Ativo';
	 
	    return $posts_columns;
	 
	}
    
	function column_cb( $item ){
	    return sprintf(
	        '<input type="checkbox" name="%1$s[]" value="%2$s" />', 
	        $this->_args['singular'],
	        $item->id
	    );
	}
	
	function column_ativo( $item ) {
	global $_omnes;
		
		if($item->ativo == 1) {
			printf('<span class="ico-ativo ativo"></span>');
		} else {
			printf('<span class="ico-ativo desativado"></span>');
		}
	}
	
	function column_nome ( $item ) {
		
		printf('<strong>%s</strong>',$item->nome);
		
		if($item->ativo == 1) {
			$actions = sprintf('<span><a href="admin.php?page=om_newsletter&action=desativar&e-mail=%s">Desativar</a></span> | ',$item->id);
		} else {
			$actions = sprintf('<span><a href="admin.php?page=om_newsletter&action=ativar&e-mail=%s">Ativar</a></span> | ',$item->id);
		}
		
		$actions .= sprintf('<span><a href="admin.php?page=om_newsletter&action=delete&e-mail=%s" style="color:red;">Excluir permanentemente</a></span>',$item->id);
		printf('<div class="row-actions">%s</div>',$actions);
		
	}
	
	function column_default( $item, $column_name ){
	    switch( $column_name ){
	        case 'name':
	            echo '<a href="' . esc_url( $item->permalink ) . '" target="_blank">';
	            echo $item->name;
	            echo '</a>';
	            break;

	        default: 
	        	
	        	echo $item->$column_name;
	        
	    }
	 
	}
	
	function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete',
        	'ativar' 	=> 'Ativar',
        	'desativar' => 'Desativar'
        );
        return $actions;
    }
    
    function process_bulk_action() {
    	
    	global $wpdb;
    	
        if( 'delete' === $this->current_action() ) {
        	
        	if(is_array($_GET['e-mail'])) {
        	
	        	foreach($_GET['e-mail'] as $val):
	        	
	        		$wpdb->query( 
	        			$wpdb->prepare("DELETE FROM {$wpdb->prefix}emails_newsletter WHERE id = %d",$val)
	        		);
	        	
	        	endforeach;
        	
        	} else {
        		
        		$wpdb->query( 
	        			$wpdb->prepare("DELETE FROM {$wpdb->prefix}emails_newsletter WHERE id = %d",$_GET['e-mail'])
	        	);
        		
        	}
        	
        	
        }
        
        if( 'ativar' === $this->current_action()) {
        	
        	if(is_array($_GET['e-mail'])) {
        	
	        	foreach($_GET['e-mail'] as $val):
	        	
	        		$wpdb->update("{$wpdb->prefix}emails_newsletter", array("ativo" => "1"), array("id" => $val));
	        	
	        	endforeach;
        	
        	} else {
        		
        		$wpdb->update("{$wpdb->prefix}emails_newsletter", array("ativo" => "1"), array("id" => $_GET['e-mail']));
        		
        	}
        	
        	
        }
        
        if( 'desativar' === $this->current_action()) {
        	
        	if(is_array($_GET['e-mail'])) {
        	
	        	foreach($_GET['e-mail'] as $val):
	        	
	        		$wpdb->update("{$wpdb->prefix}emails_newsletter", array("ativo" => "0"), array("id" => $val));
	        	
	        	endforeach;
        	
        	} else {
        		
        		$wpdb->update("{$wpdb->prefix}emails_newsletter", array("ativo" => "0"), array("id" => $_GET['e-mail']));
        		
        	}
        	
        	
        }
        
    }
	
    function prepare_items(){
    	
    	 $this->process_bulk_action();
    	
    	global $wpdb;
    	
    	if(isset($_GET["s"])) {
       		$s = $_GET['s'];
       		$where = "WHERE nome LIKE '%$s%' OR email LIKE '%$s%'";
       }
        
        $results = $wpdb->get_results("SELECT id,email,ativo,nome FROM {$wpdb->prefix}emails_newsletter $where");
    	
        $this->items = array();
        foreach($results as $email):
        	
        	$this->items[] = (object) array("nome" => $email->nome,"email" => $email->email, "ativo" => $email->ativo, "id" => $email->id);
        
        endforeach;

    	 $total_items = count( $this->items );
    	 $total_pages = ceil( $total_items / 20 );
    	 
    	 $this->set_pagination_args( array(
	        'total_items' => $total_items,
	        'total_pages' => $total_pages,
	        'per_page' => 20
    	 	)
        );
        
        $this->_column_headers = array( 
	        $this->get_columns(), // Vai buscar todas as colunas definidas
	        array(), // Poderá listar aqui as chaves das colunas que queremos esconder
	        $this->get_sortable_columns() // Vai buscar as colunas que são sorteáveis
        );
    	
    }
	
}
