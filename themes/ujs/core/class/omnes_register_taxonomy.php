<?php
Class omnes_register_taxonomy {
	
	var $name;
	var $singular_name;
	var $plural_name;
	var $gender;
	var $slug;
	var $voc_gender;
	var $post_type;
	private $labels;
	private $args;
	
	function __construct($name,$singular_name,$plural_name,$gender,$slug,$post_type) {
		
		$this->name = $name;
		$this->singular_name = $singular_name;
		$this->plural_name = $plural_name;
		$this->gender = $gender;
		$this->slug = $slug;
		$this->post_type = $post_type;
		
		$this->voc_gender = $this->gender == "F" ? 'a' : 'o';
		
		$this->labels = array(
				'name'                       => sprintf(__( '%s','text_domain' ),$this->name),
				'singular_name'              => sprintf(__( '%s', 'text_domain' ),$this->singular_name),
				'menu_name'                  => sprintf(__( '%s', 'text_domain' ),$this->name),
				'all_items'                  => sprintf(__( 'Tod%1$s %1$ss %2$s', 'text_domain' ),$this->voc_gender,$this->plural_name),
				'parent_item'                => sprintf(__( '%s Pai', 'text_domain' ),$this->singular_name),
				'parent_item_colon'          => sprintf(__( '%s pai:', 'text_domain' ),$this->singular_name),
				'new_item_name'              => sprintf(__( 'Nome d%s %s', 'text_domain' ),$this->voc_gender,$this->singular_name),
				'add_new_item'               => sprintf(__( 'Add nov%s %s', 'text_domain' ),$this->voc_gender,$this->singular_name),
				'edit_item'                  => sprintf(__( 'Editar %s', 'text_domain' ),$this->singular_name),
				'update_item'                => sprintf(__( 'Atualizar %s', 'text_domain' ),$this->singular_name),
				'separate_items_with_commas' => sprintf(__( 'Separar %s por vírgulas', 'text_domain' ),$this->plural_name),
				'search_items'               => sprintf(__( 'Pesquisar %s', 'text_domain' ),$this->singular_name),
				'add_or_remove_items'        => sprintf(__( 'Add ou remover %s', 'text_domain' ),$this->singular_name),
				'choose_from_most_used'      => sprintf(__( 'Escolha entre %ss mais usados', 'text_domain' ),$this->voc_gender),
		);
		
		$this->args = array(
				'labels'                     => $this->labels,
				'hierarchical'               => true,
				'public'                     => true,
				'show_ui'                    => true,
				'show_admin_column'          => true,
				'show_in_nav_menus'          => true,
				'show_tagcloud'              => true,
		);
		
		
		register_taxonomy( $this->slug, $this->post_type, $this->args );
		
	}
	
}