<?php

Class omnes_register_post_type {
	
	var $name;
	var $singular_name;
	var $plural_name;
	var $gender;
	var $slug;
	var $post_type;
	var $supports;
	var $hierarchical;
	var $public;
	var $show_ui;
	var $show_in_menu;
	var $show_in_nav_menus;
	var $show_in_admin_bar;
	var $menu_position;
	var $menu_icon;
	var $can_export;
	var $has_archive;
	var $exclude_from_search;
	var $publicly_queryable;
	var $taxonomies;
	var $rewrite;
	
	private $labels;
	private $args;
	private $voc_gender;
	
	function __construct($name,$singular_name,$plural_name,$gender,$slug) {
		
		$this->name = $name;
		$this->singular_name = $singular_name;
		$this->plural_name = $plural_name;
		$this->gender = $gender;
		$this->slug = $slug;
		$this->post_type = $post_type;
		$this->supports = array( 'title', 'editor', 'thumbnail' );
		$this->hierarchical = false;
		$this->public = true;
		$this->show_ui = true;
		$this->show_in_menu = true;
		$this->show_in_nav_menus = true;
		$this->show_in_admin_bar = true;
		$this->menu_position = '';
		$this->menu_icon = '';
		$this->can_export = true;
		$this->has_archive = true;
		$this->exclude_from_search = false;
		$this->publicly_queryable = true;
		$this->taxonomies = '';
		$this->rewrite = array('slug' => $slug, 'with_front' => true, 'pages' => true,'feeds' => true,);

		
		$this->voc_gender = $this->gender == "F" ? 'a' : 'o';
		
		$this->labels = array(
				'name'                => sprintf(__( '%s', 'yit' ),$this->name),
				'singular_name'       => sprintf(__( '%s', 'yit' ),$this->singular_name),
				'menu_name'           => sprintf(__( '%s', 'yit' ),$this->name),
				'parent_item_colon'   => sprintf(__( '%s Pai', 'yit' ),$this->singular_name),
				'all_items'           => sprintf(__( 'Tod%1$ss %1$ss %2$s', 'yit' ),$this->voc_gender,$this->plural_name),
				'view_item'           => sprintf(__( 'Visualizar %s', 'yit' ),$this->singular_name),
				'add_new_item'        => sprintf(__( 'Add Nov%1$s %2$s', 'yit' ),$this->voc_gender,$this->singular_name),
				'add_new'             => sprintf(__( 'Nov%s %s', 'yit' ),$this->voc_gender,$this->singular_name),
				'edit_item'           => sprintf(__( 'Editar %s', 'yit' ),$this->singular_name),
				'update_item'         => sprintf(__( 'Atualizar %s', 'yit' ),$this->singular_name),
				'search_items'        => sprintf(__( 'Pesquisar %s', 'yit' ),$this->singular_name),
				'not_found'           => sprintf(__( 'Não há %s', 'yit' ),$this->plural_name),
				'not_found_in_trash'  => sprintf(__( 'Não há %s na lixeira', 'yit' ),$this->plural_name),
		);
		
	}
	
	function register() {
		
		$this->args = array(
				'label'               => __( $this->name, 'yit' ),
				'description'         => __( '', 'yit' ),
				'labels'              => $this->labels,
				'supports'            => $this->supports,
				'taxonomies'          => array( 'category', 'post_tag' ),
				'hierarchical'        => $this->hierarchical,
				'public'              => $this->public,
				'show_ui'             => $this->show_ui,
				'show_in_menu'        => $this->show_in_menu,
				'show_in_nav_menus'   => $this->show_in_nav_menus,
				'show_in_admin_bar'   => $this->show_in_admin_bar,
				'menu_position'       => $this->menu_position,
				'menu_icon'           => $this->menu_icon,
				'can_export'          => $this->can_export,
				'has_archive'         => $this->has_archive,
				'exclude_from_search' => $this->exclude_from_search,
				'publicly_queryable'  => $this->publicly_queryable,
				'capability_type'     => 'post'
		);
		
		register_post_type( $this->slug, $this->args );
		
	}
	
}