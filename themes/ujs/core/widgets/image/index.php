<?php
class image_widget extends WP_Widget {

	public function image_widget() {

		parent::__construct(false, 'Imagem');

	}

	public function form($instance) {

		$defaults = array(
				"titulo" => "Imagem",
		);
		
		wp_enqueue_media();

		$instance = wp_parse_args($instance,$defaults);
		echo '
		<script type="text/javascript">
		jQuery(document).ready(function($){
			  var _custom_media = true,
			  _orig_send_attachment = wp.media.editor.send.attachment;
			
			  $("#add-image-widget").click(function(e) {
			    var send_attachment_bkp = wp.media.editor.send.attachment;
			    var button = $(this);
			    var id = button.attr("id").replace("_button", "");
			    _custom_media = true;
			    wp.media.editor.send.attachment = function(props, attachment){
			      if ( _custom_media ) {
			        $("#"+id).val(attachment.url);
			      } else {
			        return _orig_send_attachment.apply( this, [props, attachment] );
			      };
			    }
			
			    wp.media.editor.open(button);
			    return false;
			  });
			
			  $(".add_media").on("click", function(){
			    _custom_media = false;
			  });
			});
		</script>';
	
		echo 
		'
		<div class="uploader">
		  <input type="text" name="settings[image]" id="_unique_name" />
		  <input class="button" name="add-image-widget" id="add-image-widget" value="Upload" />
		</div>
		';
		
	}

	public function widget($args, $instance) {
		global $WP_Error, $post, $wpdb;

		if($_GET['action'] == "register-newsletter") {

			$new_email["post_type"] = "email-newsletter";
			$new_email["post_title"] = $_POST["email"];

			$headers = sprintf('From: %s <%s>' . "\r\n",get_option('bloginfo'),get_option('admin_email'));
			$message = sprintf('
					<p><strong>Houve uma nova incrição em sua Newsletter. Segue abaixo o nome e sobrenome:</strong> <br /><br />
					</p>
					<p>%s</p>
					<p>%s</p>
					',$_POST['email'], $_POST['nome']);

			wp_mail(get_option('admin_email'), 'Inscrição em Newsletter - '.get_option('bloginfo'), $message,$headers);

		}

		$defaults = array(
				"titulo" => "Newsletter",
				"width" => "267",
				"frase" => "Cadastre seu e-mail para receber nossas novidades:",
				"success" => "E-mail cadastrado com sucesso em nossa newsletter.",
				"duplicate" => "Este e-mail já esta cadastrado em nossa newsletter.",
				"error" => "Ocorreu um erro ao cadastrar seu e-mail em nossa newsletter."
		);

		$instance = wp_parse_args($instance,$defaults);

		$url = get_permalink()."?action=register-newsletter#form-register-newsletter";


		$html .= $args["before_widget"];

		$html .= $args["before_title"].$instance['titulo'].$args["after_title"];

		$html .= sprintf('<p class="msg">%s</p>',$instance["frase"]);

		$html .= '<form action="'.$url.'" method="POST" id="form-register-newsletter">';

		$html .= '<p><label for="email">Nome completo:</label>';

		$html .= '<input type="text"  name="nome" required="true"  /></p>';

		$html .= '<p><label for="email">E-mail:</label>';

		$html .= '<input type="text"  name="email" required="true"  /></p>';

		$style = 'width:'.$instance["width"].'px; white-space:normal; margin-bottom:10px;';

		if(isset($registrado)) {

			if($registrado) {
				$html .= sprintf('<span class="label label-success" style="%s">%s</span>',$style,$instance["success"]);
			} else {
				$html .= sprintf('<span class="label label-important" style="%s">%s</span>',$style,$instance["error"]);
			}

		} else if(isset($duplicate) AND $duplicate == true) {
			$html .= sprintf('<span class="label label-warning" style="%s">%s</span>',$style,$instance["duplicate"]);
		}



		$html .= '<button class="btn btn-danger" data-loading-text="Carregando..." >OK</button>';

		$html .= '</form>';

		$html .= $args["after_widget"];

		return $html;

	}



}

add_action('widgets_init', create_function('', 'return register_widget("image_widget");'));