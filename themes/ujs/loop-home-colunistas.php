<?php 

?>
<div class="row-fluid loop-home-colunistas">
<?php

function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}

//$users = get_users_ordered_by_post_date("role=author&category_name=artigos-de-colunistas");
$users = get_users_ordered_by_post_date($args);
//print_r($users);

//gera um array com 3 indice de colunistas que não se repetem
//$indiceColunistas = UniqueRandomNumbersWithinRange(0, count($users)-1, 3);


for ($i = 0; $i < 4; $i++) {

	//$user = $users[$indiceColunistas[$i]];
	$user = $users[$i];

	$thumb = get_user_meta($user->ID,'foto');
	$thumb = wp_get_attachment_image_src($thumb,'full');

	if($user->has_cap('author')) {

			$args=array(
			  'author' => $user->ID,
			  'post_type' => 'noticias',
			  'category_name' => 'artigos-de-colunistas', 
			  'post_status' => 'publish',
			  'posts_per_page' => 1,
			  'ignore_sticky_posts'=> 1
			);
			$my_query = null;
			$my_query = new WP_Query($args);
			if( $my_query->have_posts() ) {
			  while ($my_query->have_posts()) : $my_query->the_post(); 

?>
					<div class="item span3" >
						<div class="thumb" style="background-image:url(<?php echo $thumb[0]?>)"></div>
						<div class="content">
							<h2 class="title2"><a href="<?php echo get_author_posts_url($user->ID)?>"><?php echo $user->display_name?></a></h2>
							<!--<div class="excerpt"> -->
							<?php // echo get_excerpt(get_usermeta($user->ID,'description'),100) ?>
							<!-- </div> -->
							<div class="excerpt"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></div>
						</div>
					</div>
<?php 
			//}
			  endwhile;
			}
			wp_reset_query();

	}
}

?>
</div>
