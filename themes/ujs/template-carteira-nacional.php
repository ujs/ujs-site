<?php

/*
 * Template name: Carteira Nacional
 */
get_header();

have_posts(); the_post();

?>
<?php

/**
* Includes
*/

/*include_once("/inetpub/wwwroot/ujs/www.ujs.org.br/ujs-form/app.php");*/include_once("/inetpub/wwwroot/ujs/www.ujs.org.br/ujs-form/app.php");
include_once(SIS_PATH . "model/CadastroModel.php");
include_once(SIS_PATH . "dao/CadastroDao.php");
include_once(SIS_PATH . "inc-cabecalho.php");

// AppView

$app = new AppView();

// Vars base

$urlCtrlCadastro = SIS_URL . "controller/CadastroController.php";
$urlCtrlUsuario  = SIS_URL . "controller/UsuarioController.php";

// Obter dados
			
$obj = new CadastroModel($app->user("codCadastro"));

$objDao = new CadastroDao();
$obj = $objDao->obter($obj);

$txtEndereco	 	 = $obj->getEndereco();
$txtEnderecoNum	 	 = $obj->getEnderecoNum();
$txtEnderecoComp 	 = $obj->getEnderecoComp();
$txtBairro		 	 = $obj->getBairro();
$txtCidade		 	 = $obj->getCidade();
$txtEstado		 	 = $obj->getEstado();
$txtCep			 	 = $obj->getCep();
$txtPais		 	 = $obj->getPais();

$txtNome		 	 = $obj->getNome();
$txtEmail		 	 = $obj->getEmail();
$rdoSexo		 	 = $obj->getSexo();
$txtOrientacaoSexual = $obj->getOrientacaoSexual();
$txtDataNasc		 = $obj->getDataNascimento();
$txtTelefone		 = $obj->getTelefone();
//$txtCelular		 	 = $obj->getCelular();

$rdoSolicitacarteira = $obj->getsolicitaCarteira();

unset($obj, $objDao);

$chk = "checked=\"checked\"";

// Define display

$dspAba1 = "block";
$dspAba2 = "none";

if ($app->user("codCadastro") > 0) {
	$dspAba1 = "none";
	$dspAba2 = "block";
}

?>

<script type="text/javascript">
	$(document).ready(function() {
		
		// Disparar form (AJAX)
		// Login
		
		$("form[id^='frm-login']").submit(function(event) {
			event.preventDefault();
			var fiels = $(this).serialize();
			var _self = this;
			
			$(_self).find("#btnEntrar").val("Aguarde...").attr("disabled", true);
			$(_self).find(".erro").removeClass("erro");
			$("#div-mensagem").removeClass("msg-erro, msg-sucesso");
			$("#div-mensagem").fadeOut("fast", function() {
				$.ajax({
					url		: $(_self).attr("action"),
					data	: fiels,
					type	: "POST",
					success : function(json) { 
						if (json.et == "OK") {
							//window.location.reload();
							document.location.href="/";
							//var urlAtual = $.trim(window.location).replace("#", "");
							
							/*$("#box-aba-2").load(urlAtual + " #frm-atualizar",
						 		function(data) {
									$("#box-aba-1").fadeOut("fast", function() {
										$("div.abas li.aba-2").addClass("active");
										$("#box-aba-2").fadeIn("fast");
									});
								}
						 	);*/
						}
						else {
							$("#div-mensagem").addClass("msg-erro");
							$(_self).find("[name='" + json.ecmp + "']").addClass("erro").focus();
						}
						
						if (json.et == "ERRO") {
							$("#div-mensagem p").html(json.emsg);
							$("#div-mensagem").fadeIn("fast", function() {
								$("html, body").animate({scrollTop: ($("#div-mensagem").offset().top - 80)}, "slow");
							});
						}
						else if (json.et != "OK") alert("FALHA!");
						
						$(_self).find("#btnEntrar").val("Entrar").attr("disabled", false);
					}
				});
			});
			
			return false;
		});
		
		// Disparar form (AJAX)
		// Recuperar senha
		
		$("form[id^='frm-recuperar-senha']").submit(function(event) {
			event.preventDefault();
			var fiels = $(this).serialize();
			var _self = this;
			
			$(_self).find("#btnRecuperar").val("Aguarde...").attr("disabled", true);
			$(_self).find(".erro").removeClass("erro");
			$("#div-mensagem").removeClass("msg-erro, msg-sucesso");
			$("#div-mensagem").fadeOut("fast", function() {
				$.ajax({
					url		: $(_self).attr("action"),
					data	: fiels,
					type	: "POST",
					success : function(json) {
						$(_self).find("#btnRecuperar").val("Recuperar").attr("disabled", false);
						
						if (json.et == "OK") {
							$("#div-mensagem").addClass("msg-sucesso");
							setTimeout(function() {
								$("#btn-efetuar-login").trigger("click");
							}, 5000);
						}
						else if (json.et == "ERRO") {
							$("#div-mensagem").addClass("msg-erro");
							$(_self).find("[name='" + json.ecmp + "']").addClass("erro").focus();
						}
						else alert("FALHA!");
						
						$("#div-mensagem p").html(json.emsg);
						$("#div-mensagem").fadeIn("fast", function() {
							$("html, body").animate({scrollTop: ($("#div-mensagem").offset().top - 80)}, "slow");
						});
					}
				});
			});
			
			return false;
		});
		
		// Disparar form (AJAX)
		// Endereço de entrega
		
		$("form[id^='frm-atualizar']").submit(function(event) {

			event.preventDefault();
			var fiels = $(this).serialize();
			var _self = this;
			
			$(_self).find("#btnEnviar").val("Aguarde...").attr("disabled", true);
			$(_self).find(".erro").removeClass("erro");
			$("#div-mensagem").removeClass("msg-erro, msg-sucesso");
			$("#div-mensagem").fadeOut("fast", function() {
				$.ajax({
					url		: $(_self).attr("action"),
					data	: fiels,
					type	: "POST",
					success : function(json) { 
						
						//if (json.et == "OK") {
							var urlAtual = $.trim(window.location).replace("#", "");
							
							$("#box-endereco-entrega-mae").load(urlAtual + " #box-endereco-entrega-filha",
						 		function(data) {
									$("#box-aba-2").fadeOut("fast", function() {
										$("div.abas li.aba-4").addClass("active");
										$("#box-aba-3").fadeIn("fast");
										$("html, body").animate({scrollTop: ($("div.title-inside:first").offset().top)}, "slow");
									});
								}
						 	);
						//} 
						/*
						else {
							$("#div-mensagem").addClass("msg-erro");
							$(_self).find("[name='" + json.ecmp + "']").addClass("erro").focus();
						}
						
						if (json.et == "ERRO") {
							$("#div-mensagem p").html(json.emsg);
							$("#div-mensagem").fadeIn("fast", function() {
								$("html, body").animate({scrollTop: ($("div.title-inside:first").offset().top)}, "slow");
							});
						}
						else if (json.et != "OK") alert("FALHA!");
						*/
						
						$(_self).find("#btnEnviar").val("Enviar").attr("disabled", false);
					}
				});
			});
			
			return false;
		});
		
		// Ativar opção recuperar senha
		
		$("#btn-esqueci-minha-senha").click(function(event) {
			event.preventDefault();
			var email = $("#frm-login").find("#txtEmail").val();
			$("#frm-recuperar-senha").find("#txtEmail").val(email);
			
			$("#div-mensagem p").html("");
			$("#div-mensagem").removeClass("msg-erro, msg-sucesso").css("display", "none");
			$("form").find(".erro").removeClass("erro");
			
			$("#box-login").fadeOut("fast", function() {
				$("#box-recuperar-senha").fadeIn("fast");
			});
		});
		
		// Ativar opção efetuar login
		
		$("#btn-efetuar-login").click(function(event) {
			event.preventDefault();
			var email = $("#frm-recuperar-senha").find("#txtEmail").val();
			$("#frm-login").find("#txtEmail").val(email);
			
			$("#div-mensagem p").html("");
			$("#div-mensagem").removeClass("msg-erro, msg-sucesso").css("display", "none");
			$("form").find(".erro").removeClass("erro");
			
			$("#box-recuperar-senha").fadeOut("fast", function() {
				$("#box-login").fadeIn("fast");
			});
		});
	});
</script>

<aside>

	<div class="container page-inside template-filie-se template-carteira-nacional">
	
	<div class="title-inside">
		<h2 class="title-pages">FILIE-SE À UJS</h2>
	</div>
	
	<?php get_template_part('children','filiese')?>
	
	<h2 class="title-pages">
		<span style="width:30%;"><?php the_title()?></span>
		<div class="bg" style="width:64%"></div>
	</h2>
			
	<div class="content-post" style="margin-left:10%; display:inline-block;">
	
	<?php the_content()?>
	
		<div id="formularios">
	
			<div class="abas">
				<ul>
					<li id="identificacao" class="aba-1 active">
						<div class="number">1</div>
						<div class="title">Identificação</div>
					</li>
					<li id="endereco-entrega" class="aba-2 <?php echo ($dspAba2 == "block") ? "active" : ""; ?>">
						<div class="number">2</div>
						<div class="title">Endereço de Entrega</div>
					</li>
					<li id="sucesso" class="aba-4">
						<div class="number">3</div>
						<div class="title">Sucesso</div>
						<div class="tick"></div>
					</li>
				</ul>
			</div>
            
            <!-- INI: MENSAGEM -->
            <div id="div-mensagem" class="msg-erro">
                <p></p>
            </div>
            <!-- FIM: MENSAGEM -->

			<div class="content-formularios">
            	
                <div id="box-aba-1" class="aba-1" style="display:<?php echo $dspAba1; ?>;">
                    <div class="row-fluid">
                        <div class="span6 login">
                        	<div id="box-login">
                                <h3>Login</h3>
                                
                                <form id="frm-login" method="post" action="<?php echo $urlCtrlUsuario; ?>?acao=login" onsubmit="return false;">
                                    <input type="hidden" name="eForm" id="eForm" value="OK" />
                                    <p class="field"><label for="">E-mail:</label><input type="text" name="txtEmail" id="txtEmail" style="height:30px;"></p>
                                    <p class="field"><label for="">Senha:</label><input type="password" name="txtSenha" id="txtSenha" style="height:30px;"/></p>
                                    
                                    <input type="submit" name="btnEntrar" id="btnEntrar" value="Entrar" />
                                    <br><br>
                                    <a href="#" id="btn-esqueci-minha-senha">Esqueci minha senha</a>
                                </form>
                            </div>
                            <div id="box-recuperar-senha" style="display:none;">
                            	<h3>Recuperar senha</h3>
                            	
                                <form id="frm-recuperar-senha" method="post" action="<?php echo $urlCtrlUsuario; ?>?acao=recuperarSenha" onsubmit="return false;">
                                    <input type="hidden" name="eForm" id="eForm" value="OK" />
                                    <p class="field"><label for="">E-mail:</label><input type="text" name="txtEmail" id="txtEmail" style="height:30px;"></p>
                                    
                                    <input type="submit" name="btnRecuperar" id="btnRecuperar" value="Recuperar" />
                                    <br><br>
                                    <a href="#" id="btn-efetuar-login">Efetuar login</a>
                                </form>
                            </div>
                        </div>
                        <div class="span6 cadastro">
                            <h3>Não sou cadastrado</h3>
                            <br><br>
                            <p class="texto">
                            Preencha os dados a seguir e crie seu login. <br><br>
                            </p>
                            
                            <a href="<?php echo get_permalink(86)?>" class="cadastrar">Cadastre-se</a>
                        </div>
               		</div>
                </div>
            
            	<div id="box-aba-2" class="aba-1" style="display:<?php echo $dspAba2; ?>;">
					<form id="frm-atualizar" method="post" action="<?php echo $urlCtrlCadastro; ?>?acao=atualizarCadastro" onsubmit="return false;">
                        <div>
                        <input type="hidden" name="eForm" id="eForm" value="OK" />
                        </div>
                        
                        <div class="form">
                            <h3>Os campos marcados com * são obrigatórios</h3>
                            
                            <!-- INI: DIV BLOCO -->
                            <div class="bloco">
                            	<h4 class="first">Dados para entrega</h4>
                                
                                <div class="linha">
                                    <label for="rdoEntregarCarteiraEndereco">&nbsp;</label>
                                    <div class="campo">
                                        <input type="radio" class="rdo rdo-first" name="rdoEntregarCarteiraEndereco" id="rdoEntregarCarteiraEndereco"  <?php echo ($rdoSolicitacarteira == "S") ? $chk : ""; ?> value="S" />Entregar a Carteira Nacional do Militante neste endereço
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="linha">
                                    <label for="txtCep">CEP<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt f-cep" name="txtCep" id="txtCep" maxlength="255" value="<?php echo $txtCep; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtPais">País<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtPais" id="txtPais" maxlength="255" value="<?php echo $txtPais; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtEndereco">Endereço<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEndereco" id="txtEndereco" maxlength="255" value="<?php echo $txtEndereco; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtEnderecoNum">Número<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEnderecoNum" id="txtEnderecoNum" maxlength="255" value="<?php echo $txtEnderecoNum; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtEnderecoComp">Complemento</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEnderecoComp" id="txtEnderecoComp" maxlength="255" value="<?php echo $txtEnderecoComp; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtBairro">Bairro</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtBairro" id="txtBairro" maxlength="255" value="<?php echo $txtBairro; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtCidade">Cidade<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtCidade" id="txtCidade" maxlength="255" value="<?php echo $txtCidade; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtEstado">Estado<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEstado" id="txtEstado" maxlength="255" value="<?php echo $txtEstado; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
								
								<div class="linha">
                                    <label for="txtTelefone">Telefone/Celular<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt f-telefone" name="txtTelefone" id="txtTelefone" maxlength="255" value="<?php echo $txtTelefone; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha" style="display: none;">
                                    <label for="txtCelular">Celular<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt f-telefone" name="txtCelular" id="txtCelular" maxlength="255" value="<?php echo $txtCelular; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
								<div style="display: none;">
                                <h4>Dados pessoais</h4>
                                
                                <div class="linha">
                                    <label for="txtNome">Nome<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtNome" id="txtNome" maxlength="255" value="<?php echo $txtNome; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtEmail">E-mail<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEmail" id="txtEmail" maxlength="255" value="<?php echo $txtEmail; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="rdoSexo">Sexo<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="radio" class="rdo rdo-first" name="rdoSexo" id="rdoSexo" value="M" <?php echo ($rdoSexo == "M") ? $chk : ""; ?> /> Masculino
                                        <input type="radio" class="rdo" name="rdoSexo" id="rdoSexo" value="F" <?php echo ($rdoSexo == "F") ? $chk : ""; ?> /> Feminino
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtOrientacaoSexual">Orientação sexual</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtOrientacaoSexual" id="txtOrientacaoSexual" maxlength="255" value="<?php echo $txtOrientacaoSexual; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtDataNasc">Data de nascimento<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt txt-p f-data" name="txtDataNasc" id="txtDataNasc" maxlength="255" value="<?php echo $txtDataNasc; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <!--<div class="linha">
                                    <label for="txtTelefone">Telefone<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt f-telefone" name="txtTelefone" id="txtTelefone" maxlength="255" value="<?php echo $txtTelefone; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtCelular">Celular<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt f-telefone" name="txtCelular" id="txtCelular" maxlength="255" value="<?php echo $txtCelular; ?>" />
                                    </div>
                                    <div class="clear"></div>
                                </div>-->
                                <div class="linha">
                                    <label for="txtSenha">Senha</label>
                                    <div class="campo">
                                        <input type="password" class="txt txt-p" name="txtSenha" id="txtSenha" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtSenhaC">Confirme sua senha</label>
                                    <div class="campo">
                                        <input type="password" class="txt txt-p" name="txtSenhaC" id="txtSenhaC" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                </div>
                                <div class="div-btn">
                                    <input type="submit" class="btn-form" name="btnEnviar" id="btnEnviar" value="Enviar" />
                                </div>
                                
                            </div>
                            <div class="clear"></div>
                            <!-- FIM: DIV BLOCO -->
                            
                        </div>
                        
                    </form>
				</div>
            	
                <div id="box-aba-3" class="content-success row-fluid" style="display:none; margin-bottom:30px;">
					<div id="box-endereco-entrega-mae" class="span6 dados">
                    	<div id="box-endereco-entrega-filha">
                            <h3>Endereço de Entrega</h3>
                            
                            <p><label for="">País</label>
                            <span class="value"><?php echo $txtPais; ?></span></p>
                            
                            <p><label for="">CEP</label>
                            <span class="value"><?php echo $txtCep; ?></span></p>
                            
                            <p><label for="">Endereço</label>
                            <span class="value"><?php echo $txtEndereco; ?></span></p>
                            
                            <p><label for="">Número</label>
                            <span class="value"><?php echo $txtEnderecoNum; ?></span></p>
                            
                            <p><label for="">Complemento</label>
                            <span class="value"><?php echo $txtEnderecoComp; ?></span></p>
                            
                            <p><label for="">Bairro</label>
                            <span class="value"><?php echo $txtBairro; ?></span></p>
                            
                            <p><label for="">Cidade</label>
                            <span class="value"><?php echo $txtCidade; ?></span></p>
                            
                            <p><label for="">Estado</label>
                            <span class="value"><?php echo $txtEstado; ?></span></p>
                        </div>
					</div>
					
					<div class="span6" style="text-align:left;">
						<h3>Forma de contribuição</h3>
						<p>Doação - PagSeguro</p>
						<p>&nbsp;</p>

                        <!-- INICIO FORMULARIO BOTAO PAGSEGURO -->
                        <form target="pagseguro" action="https://pagseguro.uol.com.br/checkout/v2/donation.html" method="post">
                        <!-- NÃO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
                        <input type="hidden" name="receiverEmail" value="financas@ujs.org.br" />
                        <input type="hidden" name="currency" value="BRL" />
                        <input type="image" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/doacoes/209x48-doar-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!" />
                        </form>
                        <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
					
					</div>
					
				</div>
				
			</div>
		
		</div>
	
	</div>

</aside>

<?php 
get_footer();
?>