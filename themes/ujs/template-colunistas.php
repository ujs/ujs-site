<?php
/*
 * Template name: Colunistas
 */

global $post,$paged;

get_header();
//have_posts(); the_post();


?>

<aside>

	<div class="container single-page template-noticias">
		
		<div class="title-inside">
			<h2 class="title-pages">Notícias</h2>
		</div>
		
		
		<div class="content-padding">

			<div class="row-fluid">
				<div class="span9">
					<h2 class="title-pages">
						<span style="width:15%;">Colunistas</span>
						<div class="bg" style="width:84%"></div>
					</h2>
				
					<?php get_template_part("loop","colunistas")?>
					
					<div id="seja-um-colunista">
						<img src="<?php echo get_assets('img', 'seja-colunista.png')?>" alt="" />
						<p id="txt">
						Envie um e-mail para <a href="mailto:querosercolunista@ujs.com.br">querosercolunista@ujs.com.br</a> com<br />
					    seu nome, idade, sexo, cidade, estado e profissão conforme <a href="http://ujs.org.br/site/sejaumcolunista.docx" target="_blank">modelo</a>. </p>
					</div>
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>