<?php
/*
 * Template name: Linha do tempo
 */


global $post;


get_header();

have_posts(); the_post();


?>

<script type="text/javascript">
jQuery(function(){
timeline();
});

function leiaMais(name){
	obj = document.getElementById(name);
	if (obj.style.display == 'none'){
			obj.style.display = 'block';
	} else {
			obj.style.display = 'none';
	}
}
</script>

<aside>

	<div class="container page-inside template-timeline ">
	
	<div class="title-inside">
		<h2 class="title-pages">SOBREA A UJS</h2>
	</div>
	
	<?php get_template_part('children','sobre')?>
	
	<h2 class="title-pages">
		<span style="width:15%;"><?php the_title()?></span>
		<div class="bg" style="width:83%"></div>
	</h2>
	
	<div class="timeline">
	
		<?php 
		$query = new WP_Query(array(
				"post_type" => "timeline",
				"orderby" => "title",
				"order" => "ASC",
				"posts_per_page" => "-1"
				));
		
		$i = 1;
		$anos = array();
		while($query->have_posts()): $query->the_post();
		
		if(1 == $i) {
			printf('<div class="thumb"><img src="%s" alt="" /></div>',get_url_thumbnail($post->ID, 'full'));
		}
		
		$content = get_the_content();
		$ano = get_the_title();
		$thumb = get_url_thumbnail(get_the_ID(), 'full');
		
		$anos[] = array($content,$ano,$thumb);
		
		$i++;
		endwhile;

		?>
		
		<div class="anos">
			<ul>
				<?php 
				$count = count($anos);
				for($i=0;$i<$count;$i++) {
					$active = (0 == $i) ? 'active' : '';
					
					if(0 == $i) {
						$content = $anos[$i][0];
					}
					
					printf('<li data-ano="%1$s" data-thumb="%4$s" class="%3$s">%1$s <p class="content">%2$s</p> </li>',$anos[$i][1],$anos[$i][0],$active,$anos[$i][2]);
				}
				?>
			</ul>
			<div class="content-active"><?php echo $content;?></div>
		</div>
	</div>

	<br class="both" />	
			
		<?php get_template_part('template','tarja-filiese')?>
	
	</div>

</aside>

<script>
var read_year = function(btn){
}
jQuery(function($) {
	read_year = function(btn){
		var div = $(btn).find(".read_more_year").remove();
		if(div && div.length>0){
			$(btn).parent().append(div);
		}else{
			console.log("Nao achou");
			div = $(btn).parent().find(".read_more_year");
		}
		console.log(div);
		div.toggle();
	}
});
</script>

<?php 
add_action( 'wp_enqueue_script', 'load_jquery' );
function load_jquery() {
    wp_enqueue_script( 'jquery' );
}
get_footer();
?>