<?php
function widget_rodape_widget ($atts) {

	$menu = wp_nav_menu(
			array(
					'theme_location' => 'rodape-widget',
					'container'      => false,
					'menu_class'     => '_nav sf-menu',
					'echo'			 => false
			)
	);

	$menu .= '<p style="color:#fff;">União da Juventude Socialista | ' . $atts["a"]. '</p>';

	$menu .= '<hr style="background:none; border:1px dotted #fff" />';
	
	
	
// 	$menu .= '<div id="imagens">';
	
// 	$menu .= sprintf('<p><img src="%s" alt="" /></p>',get_assets('img', 'logo-pagseguro.png'));
	
// 	$menu .= sprintf('<p><img src="%s" alt="" /></p>',get_assets('img', 'logo-cc.png'));
	
// 	$menu .= '</div>';
	
	return $menu;
	
}

add_shortcode('rodape_widget', 'widget_rodape_widget');