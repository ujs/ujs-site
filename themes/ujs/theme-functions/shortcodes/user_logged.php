<?php
function sc_user_logged() {
	$user_id = get_current_user_id();
	
	
	if(!$user_id AND is_page()) {
		printf('<script>location.href="%s";</script>',get_bloginfo('url')."/wp-login.php");
	}
}
add_shortcode('has_user_logged', 'sc_user_logged');