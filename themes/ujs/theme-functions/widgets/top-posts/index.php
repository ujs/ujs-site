<?php
class top_posts_widget extends WP_Widget {

	public function top_posts_widget() {

		parent::__construct(false, 'Posts mais lidos');

	}

	public function form($instance) {

		
		printf('<p><label for="">Título</label><input type="text" name="%s" value="%s" /></p>',$this->get_field_name('titulo'),$instance["titulo"]);

		
	}

	public function widget($args, $instance) {
		global $WP_Error, $post, $wpdb;
		

		$html .= $args["before_widget"];
		
		$html .= $args["before_title"].$instance["titulo"].$args["after_title"];
		
		$html .= '<ul class="last-posts">';
		
		
		$posts = get_posts_top_acess(5);
		
		foreach($posts as $key => $post): 
			
			$post = get_post($post->post_id);
		
			$html .= sprintf('<li><a href="%s">%s</a></li>',get_permalink($post->ID),$post->post_title);
			
		endforeach;
		
		wp_reset_query();
		
		
		$html .'</ul>';
		
		
		$html .= $args["after_widget"];

		echo $html;
	}
	
	public function update($nova_instancia, $instancia_antiga) {
		$instancia = array_merge($instancia_antiga, $nova_instancia);
	
		return $instancia;
	}



}

add_action('widgets_init', create_function('', 'return register_widget("top_posts_widget");'));