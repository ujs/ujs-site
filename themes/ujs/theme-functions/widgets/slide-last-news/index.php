<?php
class slide_last_news_widget extends WP_Widget {

	public function slide_last_news_widget() {

		parent::__construct(false, 'Slide últimas notícias');

	}

	public function form($instance) {



		
	}

	public function widget($args, $instance) {
		global $WP_Error, $post, $wpdb;
		
		
		$query = new WP_Query(array(
				"post_type" => "noticias",
				"posts_per_page" => 3,
				"tax_query" => array(
						array(
								"taxonomy" => "category",
								"field" => "id",
								"terms" => array(16)
								)
						)
				));
		
		$html .= $args["before_widget"];
		
		$html .= '<div class="slide-last-news">';
		
		$html .= '<div class="slide-last-news-content">';
		
		while($query->have_posts()): $query->the_post();
		
			$html .= sprintf('
					
					<div class="item row-fluid">
						<div class="thumb span7" style="background-image:url(%s)"></div>
						<div class="content span5">
							<a href="%s">
							<h3 class="title">%s</h3>
							<div class="time">%s</div>
							<div class="excerpt">%s</div>
							</a>
							
						</div>
					</div>
					
					',
					get_url_thumbnail(get_the_ID(), 'full'),
					get_permalink($post->ID),
					$post->post_title,
					get_the_time("d/m/Y"),
					get_excerpt($post->post_content,'300'),
					get_share_post()
					);
		
		
		endwhile;
		
		
		$html .= '</div>';
		
		$html .= '</div>';
		
		
		$html .= $args["after_widget"];
		
		echo $html;
	}



}

add_action('widgets_init', create_function('', 'return register_widget("slide_last_news_widget");'));