<?php
class material_download_widget extends WP_Widget {

	public function material_download_widget() {

		parent::__construct(false, 'Materiais de Download');

	}

	public function form($instance) {

		
		printf('<p><label for="">Título</label><input type="text" name="%s" value="%s" /></p>',$this->get_field_name('titulo'),$instance["titulo"]);

		
	}

	public function widget($args, $instance) {
		global $WP_Error, $post, $wpdb;
		

		$html .= $args["before_widget"];
		
		$html .= $args["before_title"].$instance["titulo"].$args["after_title"];
		
		$html .= '<div class="list-categoria-direcao">';
		
		$terms = get_terms('material-download');
		
		$html .= '<ul>';
		
		foreach($terms as $key => $term) {
			
			$html .= sprintf('<li><a href="%s">%s (%s)</a></li>',get_term_link($term),$term->name,$term->count);
			
		}
		
		$html .= '</ul>';
		
		$html .= '</div>';
		
		
		$html .= $args["after_widget"];

		echo $html;
	}
	
	public function update($nova_instancia, $instancia_antiga) {
		$instancia = array_merge($instancia_antiga, $nova_instancia);
	
		return $instancia;
	}



}

add_action('widgets_init', create_function('', 'return register_widget("material_download_widget");'));