<?php
class agenda_widget extends WP_Widget {

	public function agenda_widget() {

		parent::__construct(false, 'Agenda');

	}

	public function form($instance) {

		
		printf('<p><label for="">Título</label><input type="text" name="%s" value="%s" /></p>',$this->get_field_name('titulo'),$instance["titulo"]);

		
	}

	public function widget($args, $instance) {
		global $WP_Error, $post, $wpdb;
		

		$html .= $args["before_widget"];
		
		$html .= $args["before_title"].$instance["titulo"].$args["after_title"];
		
		$html .= '<div class="list-agenda">';
		
		$query = new WP_Query(array(
				"post_type" => "agenda",
				"posts_per_page" => 4,
				"meta_key" => "data",
				"orderby" => "meta_value",
				"order" => "ASC"
		));
		
		while($query->have_posts()):  $query->the_post();
		
			$content = sprintf('<div class="date">%s</div>',date("d",strtotime(get_post_meta($post->ID,'data',true))));
			$content .= sprintf('<div class="excerpt"><a href="%s">%s</a></div>',get_permalink($post->ID),get_excerpt($post->post_content,50));
		
			$html .= sprintf('<div class="item">%s</div>',$content);
		
		endwhile;
		
		
		$html .'</div>';
		
		
		$html .= $args["after_widget"];

		echo $html;
	}
	
	public function update($nova_instancia, $instancia_antiga) {
		$instancia = array_merge($instancia_antiga, $nova_instancia);
	
		return $instancia;
	}



}

add_action('widgets_init', create_function('', 'return register_widget("agenda_widget");'));