﻿<?php
global $post;
if(isset($_GET['download'])) {
	
	/*set_time_limit(0);
	
	
	$downloads = get_post_meta($post->ID,'downloads',true);
	
	if(!empty($downloads)) {
		$downloads++;
	} else {
		$downloads = 1;
	}
	
	add_post_meta($post->ID, 'downloads', $downloads,true) or update_post_meta($post->ID, 'downloads', $downloads);
	
	$arquivo = get_post_meta($post->ID,'arquivo',true);
	*/
	$arquivo = wp_get_attachment_url($_GET['download']);
	header("Content-Type: application/force-download"); 
	header("Content-disposition: attachment; filename=".$arquivo.""); 
	readfile($arquivo);
	exit;
}

get_header();
have_posts(); the_post();
?>

<script type="text/javascript">
jQuery(function(){

});
</script>

<aside>

	<div class="container page-inside template-downloads">
	
	<div class="title-inside">
		<h2 class="title-pages">Formação</h2>
	</div>
	
	<div class="content row-fluid">
	
		<div class="span9">
			<?php the_content()?>
			
			
			<div class="format-download">
				<div class="format"><?php the_terms($post->ID, 'formato-formacao')?></div>
				<a class="download" href="?download=<?=get_post_meta($post->ID,'arquivo',true)?>" target="_blank"></a>
			</div>
			
			<br />
			<br />
			<br />	

			<h2 class="title-pages">
						
						<div class="bg" style="width:98%"><a href="<?php echo get_post_type_archive_link('formacao')?>">Voltar</a></div>
					</h2>

			
		</div>
		
		<div class="span3 sidebar"><?php dynamic_sidebar('sidebar-formacao')?></div>
	
	</div>
	
		

	</div>

</aside>


<?php 
get_footer();
?>