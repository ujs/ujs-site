<?php

get_header();
global $author,$paged;

$_author = get_author_name($author);

?>

<aside>

	<div class="container single-page template-noticias">
		
		<div class="title-inside">
			<h2 class="title-pages">Notícias</h2>
		</div>
		
		
		<div class="content-padding">

			<div class="row-fluid">
				
				<div class="span9">
				
				<h2 class="title-pages">
					<span style="width:15%;">Colunistas</span>
					<div class="bg" style="width:84%"></div>
				</h2>
				
				<?php 
				$thumb = get_usermeta($author,'foto');
				$thumb = wp_get_attachment_image_src($thumb);
				?>
				<div class="the-colunista row-fluid">
					<div class="span3">
					<div class="thumb"  style="background:url(<?php echo $thumb[0]?>)">
					</div>
					<div class="name"><?php echo get_author_name($author)?></div>
					</div>
					<div class="excerpt span9">
					<?php echo get_usermeta($author,'description')?>
					</div>
				</div>
				

				<div class="loop-noticias">
					<?php 

					while(have_posts()): the_post();
					?>
						
						<div class="item">
							<time><?php the_time('d/m/Y')?></time>
							<h3 class="title"><a href="<?php the_permalink()?>"><?php the_title();?></a></h3>
							<div class="excerpt"><?php echo get_excerpt(get_the_content(),100)?></div>
						</div>
					
					<?php 
					endwhile;
					?>
				</div>
				
				<?php get_template_part('paginacao')?>
				
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>