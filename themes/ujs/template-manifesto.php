<?php
/*
 * Template name: Manifesto
 */


global $post;


get_header();

have_posts(); the_post();


?>



<aside>

	<div class="container page-inside template-manifesto">
	
	<div class="title-inside">
		<h2 class="title-pages"><?php the_title()?></h2>
	</div>
	
	<div class="content">
	<br class="both" />
	<?php the_content()?>
	</div>
	
	<br class="both" />	
			
		<?php get_template_part('template','tarja-filiese')?>
	
	</div>

</aside>


<?php 
get_footer();
?>