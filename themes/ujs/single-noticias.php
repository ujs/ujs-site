<?php
global $author;
get_header()
?>

<aside>

	<div class="container single-page template-noticias single-noticias">
	
		<div class="title-inside">
			<h2 class="title-pages">Notícias</h2>
		</div>
		
		<div class="content-padding">
		
			<div class="row-fluid">
				
				<div class="span9">
				
				<?php if(have_posts()): the_post()?>
					
					<div class="row-fluid">
						
						<div class="span2 time">
							<p id="day"><?php the_time('d')?></p>
							<p id="month"><?php the_time('M')?></p>
							<p id="year"><?php the_time('Y')?></p>
						</div>
						
						<div class="span10 content-post">
							<h2 class="title"><?php the_title()?></h2>
							
							<?php 
							if(has_post_thumbnail()) {
							?>
								<p><?php the_post_thumbnail('full');?></p>
							<?php 
							}
							?>

							<?php the_content()?>
							
							<p id="author-category">
								
								Por <?php the_author_posts_link()?><?php echo the_terms(get_the_ID(), 'categoria-noticia','' ,', ')?>
							</p>
							
							<p id="tags">
								Tags: <?php the_tags('',', ')?>
							</p>
							
							<?php comments_template()?>
							
						</div>
					
					</div>
				
				<?php endif;?>
				
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>