<?php
global $post_type;
get_header()
?>

<aside>

	<div class="container single-page">
		
		<div class="content-padding">
		
			<div class="row-fluid">
				
				<div class="span9">

				<h2 class="title-pages"><?php print_r($post_type)?></h2>
				
				<div class="loop-posts">
					<?php 
					while(have_posts()): the_post();
					?>
						
						<div class="item-post">
							<h3 class="title"><?php the_title();?></h3>
							<?php the_excerpt()?>
							<p><a href="<?php the_permalink()?>">Leia mais...</a></p>
						</div>
					
					<?php 
					endwhile;
					?>
				</div>
				
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>