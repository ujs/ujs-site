<?php
$posts = get_posts_top_acess(4);

foreach($posts as $key => $post) {
	
	$post = get_post($post->post_id);
?>

						<div class="item">
							<h3 class="title"><a href="<?php the_permalink()?>"><?php the_title();?></a></h3>
							<time><?php the_time('d/m/Y')?></time>
							 <div class="excerpt"><?php echo get_excerpt($post->post_content,200)?></div>
						</div>

<?php 
}