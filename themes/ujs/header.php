<?php 
global $post,$post_type;

if(is_post_type_archive()) {
	if($post_type == "downloads") {
		$menu_id = 'menu-item-16';
	} else if($post_type == "noticias") {
		$menu_id = 'menu-item-19';
	}
}
?>
<!DOCTYPE html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href="<?php echo get_assets('img', 'favicon.ico') ?>" rel="shortcut icon" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=610292118991880";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<style type="text/css">
<?php if(is_post_type_archive()) { 

	echo "#{$menu_id}  {
	background: #7f8f11;
	margin-top: -20px;
	}"; 
	
?>

<?php } else {?>
.menu-item-<?=$post->ID?> a {
background: #7f8f11;
margin-top: -20px;
}

<?php }?>
</style>


<header>

	<div id="menu-top">
      <ul id="logo2" class="visible-phone mobile_logo"><li><a href="http://ujs.org.br"><img width="80" class="visible-phone mobile_logo" src="http://ujs.org.br/site/wp-content/themes/arq/assets/img/logo_ujs_brancovazado.png"/></a></li></ul>
		
		<div class="container">
			<?php wp_nav_menu( array('menu' => 'Sociais' )); ?>

			<ul class="right">
				<li><a href="https://formacaocastroalves.com.br/">Escola de Formação Castro Alves</a></li>
				<li><a href="/contribua-com-a-ujs-2/">Contribua com a UJS</a></li>
				<li><a href="/filie-se/"><img class="btfilie" src="<?php echo get_assets('img', 'bt-filie.png')?>" alt="" /></a></li>
			</ul>
		</div>
	</div>

	<div class="container bg-header hidden-phone">
		<section>
			<a href="https://www.ujs.org.br/20-congresso-ujs/"><h1 class="logo"><?php bloginfo('blogname')?></h1></a>
		</section>
	</div>
	
	<div class="container nav-menu">

			<?php
			 wp_nav_menu(
			     array(
			         'theme_location' => 'main',
			         'container'      => false,
			          'menu_class'     => '_nav sf-menu',
			     )
			 );
			?>

			<section class="search">
				<form role="search" method="get" id="searchform" class="searchform" action="<?php bloginfo('url')?>">
					<input type="text" value="" name="s" id="s" placeholder="BUSCA">
					<input type="submit" id="searchsubmit" value="Pesquisar" >
					<div class="clearfix"></div>
				</form>
			</section>
	</div>
</header>