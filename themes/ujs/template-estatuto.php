<?php
/*
 * Template name: Estatuto
 */


global $post;


get_header();

have_posts(); the_post();


?>

<script type="text/javascript">
jQuery(function(){
timeline();
});
</script>

<aside>

	<div class="container page-inside template-timeline template-estatuto">
	
	<div class="title-inside">
		<h2 class="title-pages">SOBRE A UJS</h2>
	</div>
	
	<?php get_template_part('children','sobre')?>
	
	<h2 class="title-pages">
		<span style="width:15%;"><?php the_title()?></span>
		<div class="bg" style="width:83%"></div>
	</h2>
	
	<div class="content">
	<?php the_content()?>
	</div>
	
	<br class="both" />	
			
		<?php get_template_part('template','tarja-filiese')?>
	
	</div>

</aside>


<?php 
get_footer();
?>