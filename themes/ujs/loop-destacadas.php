<div class="loop-home-news">
<div class="row-fluid">
<?php
global $dp;
$dp = (empty($dp)) ? 2 : $dp;
$query = new WP_Query(array(
			"post_type" => "noticias",
			"posts_per_page" => 3,
			"tax_query" => array(
					array(
							"taxonomy" => "category",
							"field" => "id",
							"terms" => array(16)
					)
			)
			));
$i = 1;
while($query->have_posts()): $query->the_post();
$thumb = get_url_thumbnail(get_the_ID(), 'full');


?>

	

	<div class="item span4 <? echo (4 == $i) ? 'no-margin-left' : ''  ?>" >
		<div class="thumb" style="background-image:url(<?php echo get_url_thumbnail($post->ID, 'full')?>)"></div>
		<div class="content">
			<h2 class="title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
			<div class="time"><?php the_time("d/m/Y")?></div>
			<div class="excerpt"><?php echo get_excerpt($post->post_content,190)?></div>
		</div>
		<div class="comment-count"><?php comments_number(0)?></div>
	</div>


<?php 


endwhile;
?>
</div>
</div>
