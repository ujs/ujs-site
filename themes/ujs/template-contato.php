<?php
/*
 * Template name: Contato
 */
get_header();

have_posts(); the_post();
?>

<?php



/**

* Includes

*/



include_once("/inetpub/wwwroot/ujs/www.ujs.org.br/ujs-form/app.php");

include_once(SIS_PATH . "inc-cabecalho.php");



// Vars base


$urlCtrl = SIS_URL . "controller/CadastroController.php";



?>
<aside>

	<div class="container page-inside template-contato">
	
	<div class="title-inside">
		<h2 class="title-pages"><?php the_title()?></h2>
	</div>
				
	<div class="content-post">
	
	<?php the_content()?>
	
	<div class="formulario">
			<?php if (!isset($_POST['input_1'])) {?>
			<?php }?>
			<?php echo do_shortcode(get_post_meta($post->ID,'formulario',true))?>
	</div>

	
	<div id="sepreferir">
	SE PREFERIR
	</div>
	<div id="fale-com-nucleo">Fale com o <a href="../categoria-direcao/executiva-nacional/" style="color:rgb(127, 143, 17);">Núcleo da UJS</a> no seu Estado para participar das atividades da UJS e conhecer um pouco mais sobre nossas causas.</div>
	
	</div>
	
	<br class="both" /><br /><br />
	
	</div>

</aside>

<?php 
get_footer();
?>