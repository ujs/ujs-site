<?php

global $post;


get_header();

have_posts(); the_post();


?>



<aside>

	<div class="container page page-inside">
	
	<div class="title-inside">
		<h2 class="title-pages"><?php the_title()?></h2>
	</div>
	
	<div class="content">
	<?php the_content()?>
	</div>
	
		
<br />
<br />
<br />	
	</div>

</aside>


<?php 
get_footer();
?>