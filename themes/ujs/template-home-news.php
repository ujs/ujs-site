<?php
/*
 * 
 * Template name: Home de Notícias
 * 
 */
get_header();



?>

<aside>

	<div class="container single-page template-noticias">
		
		<div class="title-inside">
			<h2 class="title-pages">Notícias</h2>
		</div>
		
		
		<div class="content-padding">

			<div class="row-fluid">
				
				<div class="span9">
				
				<h2 class="title-pages">
						<span style="width:15%;">Colunistas</span>
						<div class="bg" style="width:84%"><a href="http://ujs.org.br/?p=7213">ver todos</a></div>
					</h2>
				
					
					<?php get_template_part("loop","home-colunistas")?>
					
					<div id="seja-um-colunista">
						<img src="<?php echo get_assets('img', 'seja-colunista.png')?>" alt="" />
						<p id="txt">
						Você que administra site, blog, fan page, rádio web, canal no youtube, <br>
						perfil no twitter e quer fazer parte da nossa rede de ativistas digitais, <br>
						mande um e-mail para <a href="mailto:"ujsnacional@gmail.com">ujsnacional@gmail.com</a> com seu nome completo, <br>
						idade, cidade e o endereço eletrônico dos canais que administra.</p>
												
						<!--Envie um e-mail para <a href="mailto:querosercolunista@ujs.com.br">querosercolunista@ujs.com.br</a> com<br />
					    seu nome, idade, sexo, cidade, estado e profissão conforme <a href="http://ujs.org.br/site/sejaumcolunista.docx" target="_blank">modelo</a>. --></p>
					</div>


					
					<h2 class="title-pages">
						<span style="width:25%;">Últimas Notícias</span>
						<div class="bg" style="width:72%"><a href="http://ujs.org.br/index.php/noticias/">Ver todas</a></div>
					</h2>
                    
					<?php get_template_part("loop","destacadas")?>
                    <br class="both" />
					<?php
					$dp = 6;
					get_template_part('loop','home-news')?>
					
					<br class="both" />
					
					<h2 class="title-pages">
						<span style="width:32%;">mais lidas do mês</span>
						<div class="bg" style="width:67%"><a href="<?php echo get_permalink(3495)?>">Ver todas</a></div>
					</h2>
					
					<div class="loop-top-posts">
					<?php 
					get_template_part('loop','top-access-archive-noticias');
					?>
					</div>
					<p>&nbsp;</p>
					<div class="row-fluid">
					
						<!-- <div id="flickr" class="span6">
						<?php 
						//$img = get_photo_flickr(1);
						//printf('<img src="%s" alt="" />',$img[0]);
						?>
						</div> -->
						
						<div style="margin-left:170px;">
							<div id="youtube" class="span6">
								
								<!--<iframe width="550" height="300" src="//www.youtube.com/embed/fm1WG7pcBoU" frameborder="0" allowfullscreen></iframe>-->
								<iframe width="560" height="315" src="//www.youtube.com/embed/Jv7Lzhm_SX8" frameborder="0" allowfullscreen></iframe>
								
										<?php //$id = get_video_youtube()?>
										<?php //if($id) {?>
										<!-- <iframe style="width:70%;" height="200" src="http://www.youtube.com/embed/<?=$id?>" frameborder="0" allowfullscreen></iframe> -->
										<?php
											// } else { echo 'Erro ao acessar o youtube. Entre em contato com seu servidor de hospedagem.'; }
											?>
							</div>
						</div>

					</div>
		
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>