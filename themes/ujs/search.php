<?php

get_header();



?>

<aside>

	<div class="container single-page template-noticias">

		<div class="title-inside">
			<h2 class="title-pages">Busca</h2>
		</div>


		<div class="content-padding">

			<div class="row-fluid">

				<div class="span9">

				<h2 class="title-pages">
					<span style="width:100%;">
					<?php
					if(have_posts() == 1)
					{
					?>
					Resultados para a Palavra '<?php echo htmlentities(trim($_GET['s'])) ?>'
					<?php
					}else{
					?>
					Não há resultados para a Palavra '<?php echo htmlentities(trim($_GET['s'])) ?>'
					<?php } ?>
					</span>
				</h2>


				<div class="loop-noticias">
					<?php
					while(have_posts()): the_post();
					?>

						<div class="item">
							<h3 class="title"><a href="<?php the_permalink()?>"><?php the_title();?></a></h3>
							<div class="excerpt"><?php the_excerpt()?></div>
						</div>

					<?php
					endwhile;
					?>
				</div>

				<?php get_template_part('paginacao')?>

				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>

			</div>

		</div>

	</div>

</aside>

<?php
get_footer();
?>