<?php
/*
 * Template name: Evento
 */
get_header();

global $post;

have_posts(); the_post();
?>

<?php

/**
* Includes
*/

include_once("/inetpub/wwwroot/ujs/www.ujs.org.br/ujs-form/app.php");
include_once(SIS_PATH . "inc-cabecalho.php");
//include_once(SIS_PATH . "controller/EventoControler.php");


// Vars base

$urlCtrl = SIS_URL . "controller/EventoControler.php";
include_once(SIS_PATH . "dao/EventoDao.php");

$obj = new EventoDao();

if($obj->verificar(1,$_SESSION["usuario"]["codCadastro"]))
{
	$chkS = "checked=\"checked\"";
	$chkN = "";
}
else
{
	$chkS = "";
	$chkN = "checked=\"checked\"";
}



?>
<script type="text/javascript">
	$(document).ready(function() {
		
		// Disparar form (AJAX)
		
		$("form[id^='frm-salvar']").submit(function(event) { //alert("Em desenvolvimento, aguarde..."); return false;
			event.preventDefault();
			var fiels = $(this).serialize();
			var _self = this;
			
			$(_self).find("#btnEnviar").val("Aguarde...").attr("disabled", true);
			$(_self).find(".erro").removeClass("erro");
			$("#div-mensagem").removeClass("msg-erro, msg-sucesso");
			$("#div-mensagem").fadeOut("fast", function() {
				$.ajax({
					url		: $(_self).attr("action"),
					data	: fiels,
					type	: "POST",
					success : function(json) { console.log("json: " + json);
						$(_self).find("#btnEnviar").val("Enviar").attr("disabled", false);
						
						var template = "<div class='success' style='display: block;'><h2>Agradecemos o seu interesse no evento.</h2><p>&nbsp;</p></div><br><br><br><br><br><br>";
						
						$('#conteudo').html(template);
					}
				});
			});
			
			return false;
		});
		
		// Step anterior ou próximo
		
		$("input[type='button'][attr-id]").click(function() {
			var id = $(this).attr("attr-id");
			
			if (id == "3") $("#stepCtrl").val("FIM");
			else $("#stepCtrl").val("1");
			
			if (this.id == "btnProximo") $("div.abas li.aba-" + id).addClass("active");
			else if (this.id == "btnAnterior") {
				$("div.abas li.aba-" + (parseInt(id, 10) + 1)).removeClass("active");
			}
			
			$("div[id^='div-form-step-']:visible").fadeOut("fast", function() {
				$("#div-form-step-" + id).fadeIn("fast");
			});
		});
		
		// Ativar box info (step 3)
		
		$("p[attr-id].txt-chk").hover(function() {
			var id = $(this).attr("attr-id");
			$("#box-info-chk > div[id]").css("display", "none");
			$("#box-info-chk").css("display", "block");
			$("#box-info-chk > div[id='info-chk-" + id + "']").css("display", "block");
		}, function() {
			$("#box-info-chk").css("display", "none");
		});
	});
</script>

<aside>

		<div class="container page-inside template-filie-se">
	
	<div class="title-inside">
		<h2 class="title-pages">FILIE-SE À UJS</h2>
	</div>
	<!--aqui-->
	<?php get_template_part('children','filiese');
	
		//$reflFunc = new ReflectionFunction('function_name');
	//print $reflFunc->getFileName() . ':' . $reflFunc->getStartLine();
	?>
	<!--
	<ul id="children-page">
			<li class="active"><a href="/index.php/filie-se/meus-dados/">Meus Dados</a></li>
			<li><a href="/index.php/filie-se/filie-se-a-ujs/">carteira nacional do militante</a></li>
			<li><a href="/index.php/contribua-com-a-ujs-2/">contribua com a ujs</a></li>
	</ul>
	aqui-->
	
	<h2 class="title-pages">
		<span style="width:140px;"><?php the_title()?></span>
		<div class="bg" style="width:84%"></div>
	</h2>
			
	<div class="content-post">
    
	<?php the_content()?>
	
		<div id="formularios">
		
			<div class="success" style="display:none;">
				<?php echo get_post_meta($post->ID,'success',true);?>
			</div>
            
            <!-- INI: MENSAGEM -->
            <div id="div-mensagem" class="msg-erro">
                <p></p>
            </div>
            <!-- FIM: MENSAGEM -->
			
								<p class="texto">
					Confirme sua participação no 17° Congresso da UJS, de 22 a 25 de maio em Brasília. <br><br>
					</p>
					
			
			<div class="content-formularios">
            	
                <!-- INI: CONTEÚDO -->
                <section id="conteudo">
                    
                    <form id="frm-salvar" method="post" action="<?php echo $urlCtrl; ?>?acao=participar" onsubmit="return false;">
                    <div>
                    <input type="hidden" name="eForm" id="eForm" value="OK" />
                    <input type="hidden" name="stepCtrl" id="stepCtrl" value="1" />
                    <input type="hidden" name="codEvento" id="codEvento" value="1" />
                    </div>
					                    
                    <div class="form">
                       
                        <!-- INI: DIV BLOCO -->
                        <div class="bloco">
                            
                            <!-- ########### -->
                            <!-- INI: STEP 1 -->
                            <div id="div-form-step-1">
                                <h4 class="first">Evento</h4>
                                
                                   <div class="linha">
                                    <label for="rdoSexo">Vai participar do XVII Congresso?</label>
                                    <div class="campo">
                                        <input type="radio" class="rdo rdo-first" name="rdoCongreso" <?php  echo $chkS; ?> id="rdoCongreso" value="S" /> Sim
                                        <input type="radio" class="rdo" name="rdoCongreso" <?php  echo $chkN; ?> id="rdoCongreso" value="N" /> Não
                                    </div>
                                    <div class="clear"></div>
                                </div>
								
								<div class="div-btn">
                                    <input type="submit" class="btn-form" name="btnEnviar" id="btnEnviar" value="Enviar" />
                                </div>
                            </div>
                            <!-- FIM: STEP 1 -->
                            <!-- ########### -->

                            
                        </div>
                        <div class="clear"></div>
                        <!-- FIM: DIV BLOCO -->
                        
                    </div>
                    
                </form>
                    
                </section>
                <!-- FIM: CONTEÚDO -->
                
			</div>
		
		</div>
	

	</div>
	
	</div>

</aside>

<?php 
get_footer();
?>