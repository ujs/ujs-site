<?php

global $post,$term,$taxonomy,$wp_query;


get_header();


$_term = get_term_by('slug', $term, $taxonomy);

?>

<script type="text/javascript">
jQuery(function(){
	direcao_orderby();
});
</script>

<aside>

	<div class="container page-inside template-timeline template-direcao">
	
		<div class="title-inside">
			<h2 class="title-pages">SOBRE A UJS</h2>
		</div>
		
		<?php get_template_part('children','sobre')?>
		
		<h2 class="title-pages">
			<span style="width:10%;">Direção</span>
			<div class="bg" style="width:84%"></div>
		</h2>
		
		<h2 class="title-pages" style="color:#CD5208; font-size:26px; line-height:30px;"><?php echo $_term->name?></h2>
		
		<div class="orderby">Ordernar por: 

			<select name="orderby" id="">
				<option value="">Selecionar</option>
				<option value="funcao" <?php echo ($_GET['orderby'] == "funcao") ? 'selected="true"' : '';?> data-url="<? echo get_term_link($term,$taxonomy)?>?orderby=funcao">Função</option>
				<option value="nome" <?php echo ($_GET['orderby'] == "nome") ? 'selected="true"' : '';?> data-url="<?php echo get_term_link($term,$taxonomy) ?>?orderby=nome">Ordem Alfabética</option>

			</select>
		</div>
		
		<div class="row-fluid">
		
			<div class="span9">
			
				<div class="loop-direcao row-fluid">
	
					<?php 
					$i=1;
					$a=1;
					global $wp_query;
				
					while(have_posts()): the_post();
					echo (1 == $i) ? '<div class="row-fluid">' : '';
					?>
					
						<div class="item span4">
							<div class="thumb" style="background:url(<?php echo get_url_thumbnail(get_the_ID(), 'full')?>)"></div>
							<div class="name-cargo">
								<p class="name"><?php the_title()?></p>
								<p class="cargo"><?php echo get_post_meta(get_the_ID(),'cargo',true)?></p>
							</div>
							<div class="dados">
								
								<ul>
									<li><i class="ico" id="email"></i><a href="mailto:<?php echo get_post_meta(get_the_ID(),'e-mail',true)?>"><?php echo get_post_meta(get_the_ID(),'e-mail',true)?></a></li>
									<li><i class="ico" id="twitter"></i><?php echo get_post_meta(get_the_ID(),'twitter',true)?></li>
									<li><i class="ico" id="face"></i><?php echo get_post_meta(get_the_ID(),'facebook',true)?></li>
								</ul>
								
							</div>
						</div>
					
					<?php 
					if($i == 3 OR $a == $wp_query->post_count) {
						$i = 1;
						echo '</div>';
					} else {
						$i++;
					}
					$a++;
					endwhile;
					?>
					
					
					<div class="pagination"><?php pagination_funtion()?></div>
				
				</div>
				
				
			
			</div>
			
			<div class="span3 sidebar">
				<?php dynamic_sidebar('sidebar-direcao')?>
			</div>
			
		
		</div>
		

	
	
	</div>

</aside>


<?php 
get_footer();
?>