<?php
get_header()
?>

<aside>

	<div class="container single-page template-noticias single-noticias">
	
		<div class="title-inside">
			<h2 class="title-pages">Notícias</h2>
		</div>
		
		
		
		<div class="content-padding">
		
		<h2 class="title-pages">
		<span style="width:8%;">Agenda</span>
		<div class="bg" style="width:91%"></div>
		</h2>
		
			<div class="row-fluid">
				
				<div class="span9">
				
				<?php if(have_posts()): the_post()?>
					
					<div class="row-fluid">
						
						<div class="span2 time">
						<?php $data = get_post_meta($post->ID,'data',true)?>
							<p id="day"><?php echo date("d", strtotime($data))?></p>
							<p id="month"><?php echo date("M", strtotime($data))?></p>
							<p id="year"><?php echo date("Y", strtotime($data))?></p>
						</div>
						
						<div class="span10 content-post">
							
							<?php 
							if(has_post_thumbnail()) {
							?>
								<p><?php the_post_thumbnail('full');?></p>
							<?php 
							}
							?>

							<h2 class="title"><?php the_title()?></h2>
							<?php the_content()?>
							
							<!--<p id="confirme-presenca">
								<a href="#"><img src="<?php #echo get_assets('img', 'bt-confirme-presenca.png')?>" alt="" /></a>
							</p>-->
							
							
							<div class="share">
							<?php echo get_share_post()?>
							</div>
							
							<br class="both" />
							
							
							<p id="tags">
								
							</p>
							
							<?php comments_template()?>
							
						</div>
					
					</div>
				
				<?php endif;?>
				
				</div>
				<div class="span3"><?php dynamic_sidebar('sidebar-default')?></div>
		
			</div>
		
		</div>

	</div>

</aside>

<?php 
get_footer();
?>