<?php
/*
 * Template name: Filie-se
 */
get_header();

global $post;

have_posts(); the_post();
?>

<?php

/**
* Includes
*/

include_once("/inetpub/wwwroot/ujs/www.ujs.org.br/ujs-form/app.php");
include_once(SIS_PATH . "inc-cabecalho.php");

// Vars base

$urlCtrl = SIS_URL . "controller/CadastroController.php";

?>
<script type="text/javascript">
	$(document).ready(function() {
		
		// Disparar form (AJAX)
		
		$("form[id^='frm-salvar']").submit(function(event) { //alert("Em desenvolvimento, aguarde..."); return false;
			event.preventDefault();
			var fiels = $(this).serialize();
			var _self = this;
			
			$(_self).find("#btnEnviar").val("Aguarde...").attr("disabled", true);
			$(_self).find(".erro").removeClass("erro");
			$("#div-mensagem").removeClass("msg-erro, msg-sucesso");
			$("#div-mensagem").fadeOut("fast", function() {
				$.ajax({
					url		: $(_self).attr("action"),
					data	: fiels,
					type	: "POST",
					success : function(json) { console.log("json: " + json);						// Próximo passo
						if (json.emsg == "NEXT") {
							$(_self).find("#btnEnviar").val("Enviar").attr("disabled", false);
							$("#div-form-step-1").fadeOut("fast", function() {
								$("div.abas li.aba-2").addClass("active");
								$("#div-form-step-2").fadeIn("fast");
								$("html, body").animate({scrollTop: ($("div.title-inside:first").offset().top)}, "slow");
							});
						}
						else {
							if (json.et == "OK") {
								$("#div-form-step-3,").fadeOut("fast", function() {
									$("#conteudo").css("display", "none");
									$("div.abas li.aba-4").addClass("active");
									$("#div-form-step-4").fadeIn("fast");
									$("div.success, div.success-content").fadeIn("fast");
									$("html, body").animate({scrollTop: ($("div.title-inside:first").offset().top)}, "slow");
								});
							}
							else {
								$("#div-mensagem").addClass("msg-erro");
								$(_self).find("[name='" + json.ecmp + "']").addClass("erro").focus();
							}
							
							if (json.et == "ERRO") {
								$("#div-mensagem p").html(json.emsg);
								$("#div-mensagem").fadeIn("fast", function() {
									$("html, body").animate({scrollTop: ($("#div-mensagem").offset().top - 80)}, "slow");
								});
							}
							else if (json.et != "OK") alert("FALHA!");
						}
						
						$(_self).find("#btnEnviar").val("Enviar").attr("disabled", false);
					}
				});
			});
			
			return false;
		});
		
		// Step anterior ou próximo
		
		$("input[type='button'][attr-id]").click(function() {
			var id = $(this).attr("attr-id");
			
			if (id == "3") $("#stepCtrl").val("FIM");
			else $("#stepCtrl").val("1");
			
			if (this.id == "btnProximo") $("div.abas li.aba-" + id).addClass("active");
			else if (this.id == "btnAnterior") {
				$("div.abas li.aba-" + (parseInt(id, 10) + 1)).removeClass("active");
			}
			
			$("div[id^='div-form-step-']:visible").fadeOut("fast", function() {
				$("#div-form-step-" + id).fadeIn("fast");
			});
		});
		
		$("#btExibirEndereco").click(function(){
			$('.divEndereco').toggle();
		});
		
		// Ativar box info (step 3)
		
		$("p[attr-id].txt-chk").hover(function() {
			var id = $(this).attr("attr-id");
			$("#box-info-chk > div[id]").css("display", "none");
			$("#box-info-chk").css("display", "block");
			$("#box-info-chk > div[id='info-chk-" + id + "']").css("display", "block");
		}, function() {
			$("#box-info-chk").css("display", "none");
		});
	});
</script>

<style type=text/css>
.divEndereco{
	display: none;
}
.imgBtPlus{
	cursor: pointer;
}

</style>

<aside>

	<div class="container page-inside template-filie-se">
	
	<div class="title-inside">
		<h2 class="title-pages">FILIE-SE À UJS</h2>
	</div>
	
	<?php get_template_part('children','filiese')?>
	
	<h2 class="title-pages">
		<span style="width:7%;"><?php the_title()?></span>
		<div class="bg" style="width:84%"></div>
	</h2>
			
	<div class="content-post">
    
	<?php the_content()?>
	
		<div id="formularios">
		
			<div class="success" style="display:none;">
				<?php echo get_post_meta($post->ID,'success',true);?>
			</div>
			
			<div class="abas">
				<ul>
					<li id="perfil-social" class="aba-1 active">
						<div class="number">1</div>
						<div class="title">Perfil Social</div>
					</li>
					<li id="redes-sociais" class="aba-2">
						<div class="number">2</div>
						<div class="title">Redes Sociais</div>
					</li>
					<li id="perfil-politica" class="aba-3">
						<div class="number">3</div>
						<div class="title">Perfil Politico</div>
					</li>
					<li id="sucesso" class="aba-4">
						<div class="number">4</div>
						<div class="title">Sucesso</div>
						<div class="tick"></div>
					</li>
				</ul>
			</div>
			
			<div class="success-content" style="display:none;">
				<div class="quadros">
					<h2>Contribua com R$20,00 e adquira a Carteira Nacional do Militante</h2>
					<p>Ao colaborar com R$20,00, além de ajudar a <strong>União da Juventude Socialista</strong>, você automaticamente adquire a <strong>Carteira Nacional do Militante.</strong></p>
				</div>
				
				<div class="link">
				<a href="<?php echo get_permalink(95)?>?logado=1">Contribuir</a>
				</div>
				
				<div class="quadros">
					<h2>Contribua com qualquer valor</h2>
					<p>Colabore com a <strong>União da Juventude Socialista</strong>. Contribua com qualquer valor e ajude a continuar a luta pelo país dos nossos sonhos.</p>
				</div>
				
				<div class="link">
				<a href="<?php echo get_permalink(95)?>">Contribuir</a>
				</div>
				
				<div class="quadros">
					<h2>Não desejo contribuir agora</h2>
					<p>A luta continua. Assim que puder, não deixe de contribuir.</p>
				</div>
				
				<div class="link">
				<a href="<?php bloginfo('url')?>">Continuar</a>
				</div>
				
				<br />
				<br />
				<br />
			</div>
            
            <!-- INI: MENSAGEM -->
            <div id="div-mensagem" class="msg-erro">
                <p></p>
            </div>
            <!-- FIM: MENSAGEM -->
			
			<div class="content-formularios">
            	
                <!-- INI: CONTEÚDO -->
                <section id="conteudo">
                    
                    <form id="frm-salvar" method="post" action="<?php echo $urlCtrl; ?>?acao=salvar2Site" onsubmit="return false;">
                    <div>
                    <input type="hidden" name="eForm" id="eForm" value="OK" />
                    <input type="hidden" name="stepCtrl" id="stepCtrl" value="1" />
                    </div>
                    
                    <div class="form">
                        <h3>Os campos marcados com * são obrigatórios</h3>
                        
                        <!-- INI: DIV BLOCO -->
                        <div class="bloco">
                            
                            <!-- ########### -->
                            <!-- INI: STEP 1 -->
                            <div id="div-form-step-1">
                                <h4 class="first">Cadastro convencional</h4>
                                
                                <div class="linha">
                                    <label for="txtNome">Nome<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtNome" id="txtNome" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtEmail">E-mail<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEmail" id="txtEmail" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="rdoSexo">Sexo<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="radio" class="rdo rdo-first" name="rdoSexo" id="rdoSexo" value="M" /> Masculino
                                        <input type="radio" class="rdo" name="rdoSexo" id="rdoSexo" value="F" /> Feminino
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtOrientacaoSexual">Orientação sexual</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtOrientacaoSexual" id="txtOrientacaoSexual" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtDataNasc">Data de nascimento<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt txt-p f-data" name="txtDataNasc" id="txtDataNasc" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtTelefone">Telefone/Celular<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt f-telefone" name="txtTelefone" id="txtTelefone" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha" style="display: none;">
                                    <label for="txtCelular">Celular<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt f-telefone" name="txtCelular" id="txtCelular" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtEstado">Estado<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <!--input type="text" class="txt" name="txtEstado" id="txtEstado" maxlength="255" value="" /-->
										<select name="txtEstado" id="txtEstado" >
											<option value="AC">Acre</option>
											<option value="AL">Alagoas</option>
											<option value="AM">Amazonas</option>
											<option value="AP">Amapá</option>
											<option value="BA">Bahia</option>
											<option value="CE">Ceará</option>
											<option value="DF">Distrito Federal</option>
											<option value="ES">Espirito Santo</option>
											<option value="GO">Goiás</option>
											<option value="MA">Maranhão</option>
											<option value="MG">Minas Gerais</option>
											<option value="MS">Mato Grosso do Sul</option>
											<option value="MT">Mato Grosso</option>
											<option value="PA">Pará</option>
											<option value="PB">Paraíba</option>
											<option value="PE">Pernambuco</option>
											<option value="PI">Piauí</option>
											<option value="PR">Paraná</option>
											<option value="RJ">Rio de Janeiro</option>
											<option value="RN">Rio Grande do Norte</option>
											<option value="RO">Rondônia</option>
											<option value="RR">Roraima</option>
											<option value="RS">Rio Grande do Sul</option>
											<option value="SC">Santa Catarina</option>
											<option value="SE">Sergipe</option>
											<option value="SP">São Paulo</option>
											<option value="TO">Tocantins</option>
										</select>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtCidade">Cidade<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtCidade" id="txtCidade" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtSenha">Senha<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="password" class="txt txt-p" name="txtSenha" id="txtSenha" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtSenhaC">Confirme sua senha<span class="obrigatorio">*</span></label>
                                    <div class="campo">
                                        <input type="password" class="txt txt-p" name="txtSenhaC" id="txtSenhaC" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <h4>Endereço <a herf="javascript:void(0)" ID="btExibirEndereco"><img class="imgBtPlus" src="/ujs-form/site/img/botaoAdd.gif"/></a></h4>
                                
                                <div class="linha divEndereco">
                                    <label for="txtEndereco">Rua</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEndereco" id="txtEndereco" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha divEndereco">
                                    <label for="txtEnderecoNum">Número</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEnderecoNum" id="txtEnderecoNum" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha divEndereco">
                                    <label for="txtEnderecoComp">Complemento</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEnderecoComp" id="txtEnderecoComp" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha divEndereco">
                                    <label for="txtBairro">Bairro</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtBairro" id="txtBairro" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha divEndereco">
                                    <label for="txtCep">CEP</label>
                                    <div class="campo">
                                        <input type="text" class="txt f-cep" name="txtCep" id="txtCep" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="div-btn">
                                    <input type="submit" class="btn-form" name="btnEnviar" id="btnPróximo" value="Próximo" />
                                </div>
                            </div>
                            <!-- FIM: STEP 1 -->
                            <!-- ########### -->
                            
                            <!-- ########### -->
                            <!-- INI: STEP 2 -->
                            <div id="div-form-step-2" style="display:none;">
                                <h4 class="first">Redes sociais</h4>
                                
                                <div class="linha">
                                    <label for="txtFacebook">Facebook</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtFacebook" id="txtFacebook" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtTwitter">Twitter</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtTwitter" id="txtTwitter" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha" style="display: none;">
                                    <label for="txtSkype">Skype</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtSkype" id="txtSkype" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha" style="display: none;">
                                    <label for="txtGoogle">Google+</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtGoogle" id="txtGoogle" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha" style="display: none;">
                                    <label for="txtHangout">Hangout</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtHangout" id="txtHangout" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha" style="display: none;">
                                    <label for="txtSite">Site</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtSite" id="txtSite" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="div-btn">
                                    <input type="button" class="btn-form" attr-id="3" name="btnProximo" id="btnProximo" value="Próximo" />
                                    <input type="button" class="btn-form" attr-id="1" name="btnAnterior" id="btnAnterior" value="Anterior" />
                                </div>
                            </div>
                            <!-- FIM: STEP 2 -->
                            <!-- ########### -->
                            
                            <!-- ########### -->
                            <!-- INI: STEP 3 -->
                            <div id="div-form-step-3" style="display:none;">
                            	<div id="box-info-chk">
                                	<div id="info-chk-1">
                                    	<h2>Estudantil Securalista</h2>
                                        <p>Faça parte do movimento secundarista, na qual jovens do ensino médio lutam pelos 
                                        seus direitos ao lado da UBES e demais setores.</p>
                                    </div>
                                    
                                    <div id="info-chk-2">
                                    	<h2>Estudantil Universitário</h2>
                                        <p>Neste setor, os universitários lutam por uma educação melhor e para todos, que visa 
                                        à valorização dos jovens universitários.</p>
                                    </div>
                                    
                                    <div id="info-chk-3">
                                    	<h2>Jovem trabalhador</h2>
                                        <p>Jovens precisam de trabalho e que seus direitos sejam cumpridos. Este é o objetivo deste setor.</p>
                                    </div>
                                    
                                    <div id="info-chk-4">
                                    	<h2>Mídia</h2>
                                        <p>Jovens que lutam por liberdade e por uma mídia menos manipuladora e mais informativa.</p>
                                    </div>
                                    
                                    <div id="info-chk-5">
                                    	<h2>Hip Hop</h2>
                                        <p>Jovens lutam pela valorização da cultura hip-hop.</p>
                                    </div>
                                    
                                    <div id="info-chk-6">
                                    	<h2>Cultura</h2>
                                        <p>Jovens se unem para desenvolver trabalhos e eventos que envolvam a sociedade, e tragam à tona 
                                        a verdadeira cultura brasileira.</p>
                                    </div>
                                    
                                    <div id="info-chk-7">
                                    	<h2>Esporte</h2>
                                        <p>A UJS desenvolve trabalhos na área esportiva, que é de extrema importância para o 
                                        desenvolvimento dos jovens.</p>
                                    </div>
                                    
                                    <div id="info-chk-8">
                                    	<h2>Jovens mulheres</h2>
                                        <p>Jovens mulheres que já iniciaram sua luta contra o machismo, o sexismo e pela obtenção de 
                                        seus direitos, ao lado da UJS.</p>
                                    </div>
                                    
                                    <div id="info-chk-9">
                                    	<h2>Comunitário</h2>
                                        <p>Jovens se unem para debater problemas urbanos, e esboçar políticas públicas para 
                                        o jovem e para os dilemas do país.</p>
                                    </div>
                                    
                                    <div id="info-chk-10">
                                    	<h2>Luta antiracista</h2>
                                        <p>Jovens se unem a UJS para lutar contra todo o tipo de preconceito presente na sociedade.</p>
                                    </div>
                                    
                                    <div id="info-chk-11">
                                    	<h2>Jovens Cientistas</h2>
                                        <p>Jovens cientistas do ensino médio e universitários em iniciação científica também lutam 
                                        com a UJS por melhorias na classe científica brasileira.</p>
                                    </div>
                                    
                                    <div id="info-chk-12">
                                    	<h2>LGBT</h2>
                                        <p>Jovens se unem a UJS para lutar contra os diversos tipos de preconceitos presentes na sociedade.</p>
                                    </div>
                                    
                                    <div id="info-chk-13">
                                    	<h2>Solidariedade Internacional</h2>
                                        <p>Jovens que lutam também pelos direitos de populações que sofrem fora do nosso país.</p>
                                    </div>
                                </div>
                                
                            	<div class="linha">
                                    <label for="chkFrenteMovimento">Em qual frente/movimento<br>você deseja atuar?</label>
                                    <div class="campo">
                                        <div style="padding:0px 0px 10px 0px;">
                                            <div style="float:left; width:180px;">
                                                <p class="txt-chk" attr-id="1"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Estudantil Securalista" />Estudantil Securalista</p>
                                                <p class="txt-chk" attr-id="2"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Estudantil Universitário" />Estudantil Universitário</p>
                                                <p class="txt-chk" attr-id="3"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Jovem trabalhador" />Jovem trabalhador</p>
                                                <p class="txt-chk" attr-id="4"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Mídia" />Mídia</p>
                                                <p class="txt-chk" attr-id="5"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Hip Hop" />Hip Hop</p>
                                            </div>
                                            <div style="float:left; width:160px;">
                                                <p class="txt-chk" attr-id="6"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Cultura" />Cultura</p>
                                                <p class="txt-chk" attr-id="7"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Esporte" />Esporte</p>
                                                <p class="txt-chk" attr-id="8"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Jovens mulheres" />Jovens mulheres</p>
                                                <p class="txt-chk" attr-id="9"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Comunitário" />Comunitário</p>
                                                <p class="txt-chk" attr-id="10"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Luta antiracista" />Luta antiracista</p>
                                            </div>
                                            <div style="float:left; width:200px;">
                                                <p class="txt-chk" attr-id="11"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Jovens Cientistas" />Jovens Cientistas</p>
                                                <p class="txt-chk" attr-id="12"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="LGBT" />LGBT</p>
                                                <p class="txt-chk" attr-id="13"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Solidariedade Internacional" />Solidariedade Internacional</p>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <!--
                                <p class="txt-label">Em qual frente/movimento você deseja atuar?</p>
                                <div style="padding:0px 50px 10px 50px;">
                                    <div style="float:left; width:200px;">
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Estudantil Securalista" />Estudantil Securalista</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Estudantil Universitário" />Estudantil Universitário</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Jovem trabalhador" />Jovem trabalhador</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Mídia" />Mídia</p>
                                    </div>
                                    <div style="float:left; width:200px;">
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Hip Hop" />Hip Hop</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Cultura" />Cultura</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Esporte" />Esporte</p>
                                    </div>
                                    <div style="float:left; width:200px;">
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Jovens mulheres" />Jovens mulheres</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Comunitário" />Comunitário</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Luta antiracista" />Luta antiracista</p>
                                    </div>
                                    <div style="float:left; width:200px;">
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Jovens Cientistas" />Jovens Cientistas</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Hip Hop" />LGBT</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkFrenteMovimento[]" value="Solidariedade Internacional" />Solidariedade Internacional</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                -->
                                
                                <div class="linha">
                                    <label for="txtFrenteMovimentoOutro">Outro</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtFrenteMovimentoOutro" id="txtFrenteMovimentoOutro" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="rdoSexo">Atualmente você estuda?</label>
                                    <div class="campo">
                                        <input type="radio" class="rdo rdo-first" name="rdoAtualmenteVcEstuda" id="rdoAtualmenteVcEstuda" value="S" /> Sim
                                        <input type="radio" class="rdo" name="rdoAtualmenteVcEstuda" id="rdoAtualmenteVcEstuda" value="N" /> Não
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtEscolaIes">Caso sim, em qual<br>escola e/ou IES?</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtEscolaIes" id="txtEscolaIes" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="linha">
                                    <label for="chkLocalEstudoTrabalho">Em seu local de estudo/trabalho<br>você participa do:</label>
                                    <div class="campo">
                                        <div style="padding:0px 0px 10px 0px;">
                                            <div style="float:left; width:100px;">
                                                <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="CA" />CA</p>
                                                <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="Nenhum" />Nenhum</p>
                                            </div>
                                            <div style="float:left; width:100px;">
                                                <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="DCE" />DCE</p>
                                            </div>
                                            <div style="float:left; width:100px;">
                                                <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="Grêmio" />Grêmio</p>
                                            </div>
                                            <div style="float:left; width:100px;">
                                                <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="Sindicato" />Sindicato</p>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <!--
                                <p class="txt-label">Em seu local de estudo/trabalho você participa do:</p>
                                <div style="padding:0px 50px 10px 50px;">
                                    <div style="float:left; width:200px;">
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="CA" />CA</p>
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="Nenhum" />Nenhum</p>
                                    </div>
                                    <div style="float:left; width:200px;">
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="DCE" />DCE</p>
                                    </div>
                                    <div style="float:left; width:200px;">
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="Grêmio" />Grêmio</p>
                                    </div>
                                    <div style="float:left; width:200px;">
                                        <p class="txt-chk"><input type="checkbox" class="chk" name="chkLocalEstudoTrabalho[]" value="Sindicato" />Sindicato</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                -->
                                
                                <div class="linha">
                                    <label for="txtLocalEstudoTrabalhoOutro">Outro</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtLocalEstudoTrabalhoOutro" id="txtLocalEstudoTrabalhoOutro" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="rdoSexo">Milita ou possui mandato em alguma entidade, partido ou ONG?</label>
                                    <div class="campo">
                                        <input type="radio" class="rdo rdo-first" name="rdoMilitaPossuiMandato" id="rdoMilitaPossuiMandato" value="S" /> Sim
                                        <input type="radio" class="rdo" name="rdoMilitaPossuiMandato" id="rdoMilitaPossuiMandato" value="N" /> Não
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtMilitaPossuiMandatoQual">Qual?</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtMilitaPossuiMandatoQual" id="txtMilitaPossuiMandatoQual" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="slcParticipaInstancia">Participa de alguma instância de Direção da UJS?</label>
                                    <div class="campo">
                                        <select class="slc slc-p" name="slcParticipaInstancia" id="slcParticipaInstancia">
                                            <option value="">Selecione</option>
                                            <option value="Núcleo">Núcleo</option>
                                            <option value="Direção municipal">Direção municipal</option>
                                            <option value="Estadual">Estadual</option>
                                            <option value="Nacional">Nacional</option>
                                            <option value="Nenhuma">Nenhuma</option>
                                        </select>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="rdoVcVota">Você vota?</label>
                                    <div class="campo">
                                        <input type="radio" class="rdo rdo-first" name="rdoVcVota" id="rdoVcVota" value="S" /> Sim
                                        <input type="radio" class="rdo" name="rdoVcVota" id="rdoVcVota" value="N" /> Não
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="linha">
                                    <label for="txtVcVotaQualMunicipio">Em qual município?</label>
                                    <div class="campo">
                                        <input type="text" class="txt" name="txtVcVotaQualMunicipio" id="txtVcVotaQualMunicipio" maxlength="255" value="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="div-btn">
                                    <input type="submit" class="btn-form" name="btnEnviar" id="btnEnviar" value="Enviar" />
                                    <input type="button" class="btn-form" attr-id="2" name="btnAnterior" id="btnAnterior" value="Anterior" />
                                </div>
                            </div>
                            <!-- FIM: STEP 3 -->
                            <!-- ########### -->
                            
                        </div>
                        <div class="clear"></div>
                        <!-- FIM: DIV BLOCO -->
                        
                    </div>
                    
                </form>
                    
                </section>
                <!-- FIM: CONTEÚDO -->
                
			</div>
		
		</div>
	

	</div>
	
	</div>

</aside>

<?php 
get_footer();
?>