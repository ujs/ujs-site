<?php 

?>
<div class="row-fluid loop-home-colunistas">
<?php
$users = get_users();
$first = true;
$i = 1;
foreach ($users as $key => $user) {
	$thumb = get_usermeta($user->ID,'foto');
	$thumb = wp_get_attachment_image_src($thumb,'full');
	if($user->has_cap('author')) {
?>
			<div class="the-colunista row-fluid">
				<div class="span4">
					<div class="thumb" style="background-image:url(<?php echo $thumb[0]?>)"></div>
					<div class="name"><a href="<?php echo get_author_posts_url($user->ID)?>"><?php echo $user->display_name?></a></div>
				</div>
				<div class="content">
					<div class="excerpt span8"><?php echo get_excerpt(get_usermeta($user->ID,'description'))?></div>
				</div>	
			</div>
			<br>

<?php 
		$first = false;
		$i++;
	}
}
?>
</div>
<!--

								<div class="the-colunista row-fluid">
					<div class="span3">
					<div class="thumb"  style="background:url(http://ujs.org.br/site/wp-content/uploads/2013/12/eduardo_guimaraes-150x90.jpg)">
					</div>
					<div class="name">Eduardo Guimarães</div>
					</div>
					<div class="excerpt span9">
					Comerciante, blogueiro e ativista político (presidente do Movimento dos Sem Mídia).
http://www.blogdacidadania.com.br/					</div>
				</div>
-->

