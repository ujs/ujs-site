<?php
get_header()
?>

<aside class="home-page">

	<div class="container">

		<div id="home-destaques">
			<?php dynamic_sidebar('sidebar-home')?>
		</div>

		<div class="content-padding">

			<h2 class="title-pages">
				<span style="width:22%;">últimas notícias</span>
				<div class="bg" style="width:77%"><a href="<?php echo get_post_type_archive_link('noticias')?>">ver todas</a></div>
			</h2>

			<?php get_template_part('loop','home-news')?>

			<br />

			<div class="row-fluid">
					<h2 class="title-pages">
						<span style="width:22%;">colunistas</span>
						<!--<div class="bg" style="width:77%"><a href="http://ujs.org.br/?p=7213">ver todos</a></div>-->
						<div class="bg" style="width:77%"><a href="http://ujs.org.br/index.php/auhtors/">ver todos</a></div>
					</h2>
					<?php get_template_part("loop","home-colunistas")?>
					<!--
					<h2 class="title-pages">
						<span style="width:22%;">agenda</span>
						<div class="bg" style="width:77%"><a href="#">ver todas</a></div>
					</h2>
					-->
					<?php //get_template_part('loop','home-agenda')?>
				<!--<div class="span3 sidebar-home2"><?php dynamic_sidebar('sidebar-home2')?></div>-->
			</div>

			<br/>
			
			<div class="row-fluid">
				<div class="span9">
					<?php get_template_part('loop','home-agenda')?>
				</div>
				<div class="span3 sidebar-home2"><?php dynamic_sidebar('sidebar-home2')?></div>
			</div>




			<br />

			 <h2 class="title-pages">
				<span style="width:22%;">mais lidas do mês</span>
				<div class="bg" style="width:77%"><a href="<?php echo get_permalink(6114)?>">ver todas</a></div>
			 </h2>

			<?php get_template_part('loop','home-top-acess')?>
		</div>


	</div>

</aside>

<?php
get_footer();
?>
