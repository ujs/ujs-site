<?php
if(isset($_GET['download'])) {

/*
	set_time_limit(0);


	$downloads = get_post_meta($post->ID,'downloads',true);

	if(!empty($downloads)) {
		$downloads++;
	} else {
		$downloads = 1;
	}

	add_post_meta($post->ID, 'downloads', $downloads,true) or update_post_meta($post->ID, 'downloads', $downloads);


	$arquivo = wp_get_attachment_url($_GET['download']);


	header("Location: $arquivo");

	exit;
*/
	$arquivo = wp_get_attachment_url($_GET['download']);

	header("Content-Type: application/force-download"); 
	header("Content-disposition: attachment; filename=".$arquivo.""); 
	readfile($arquivo);

	exit;	
}

get_header();

?>

<script type="text/javascript">
jQuery(function(){
	direcao_orderby();
});
</script>

<aside>

	<div class="container page-inside template-downloads">
	
	<div class="title-inside">
		<h2 class="title-pages">Downloads</h2>
	</div>

	
	<div class="content row-fluid">
	
		<div class="span9">
		
			<div class="orderby">Ordernar por: 
				<select name="orderby" id="">
					<option value="">Selecionar</option>
					<option value="nome" <?php echo ($_GET['orderby'] == "nome") ? 'selected="true"' : '';?> data-url="<?php echo get_post_type_archive_link('downloads')?>?orderby=nome">Ordem Alfabética</option>
					<option value="popular" <?php echo ($_GET['orderby'] == "popular") ? 'selected="true"' : '';?> data-url="<?php echo get_post_type_archive_link('downloads')?>?orderby=popular">Populares</option>
					<option value="data" <?php echo ($_GET['orderby'] == "data") ? 'selected="true"' : '';?> data-url="<?php echo get_post_type_archive_link('downloads')?>?orderby=data">Data de publicação</option>
				</select>
			</div>
				
			<div class="loop-downloads">
				<div class="row-fluid">
				<?php
				global $wp_query;
			
				$i=1;
				while(have_posts()): the_post();
				?>
				
					<div class="item span4">
						
						<div class="thumb" style="background:url(<?php echo get_url_thumbnail($post->ID, 'full')?>)"></div>
						<div class="the-content">
							<h3 class="title"><?php the_title()?></h3>
							<div class="excerpt">
							<a href="<?php the_permalink()?>"><?php echo get_excerpt($post->post_content,150)?></a>
							</div>
							
							<div class="format-download">
								<div class="format"><?php the_terms($post->ID, 'formato-download')?></div>
								<a class="download" href="?download=<?=get_post_meta($post->ID,'arquivo',true)?>" target="_blank"></a>
							</div>
							
						</div>
					
					</div>
				
				<?php 
				if($i % 3 == 0) {
					echo '</div><div class="row-fluid">';
				}
				$i++;
				endwhile;
				?>
				
				</div>
			
				<div class="pagination"><?php pagination_funtion($query->max_num_pages)?></div>
			
			</div>
		</div>
		
		<div class="span3 sidebar"><?php dynamic_sidebar('sidebar-downloads')?></div>
	
	</div>
	
		
<br />
<br />
<br />	
	</div>

</aside>


<?php 
get_footer();
?>