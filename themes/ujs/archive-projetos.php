<?php
/*
 * Template name: Projetos
 * 
 * 
 */
get_header()
?>

<aside>

	<div class="container page-inside">
	
		<h2 class="title-pages">Projetos</h2>
				
		<div class="loop-inside">
			<?php 
			$query = new WP_Query(array("post_type" => "projetos"));
			$i = 1;
			while($query->have_posts()): $query->the_post();
			$thumb = get_url_thumbnail(get_the_ID(), 'full');
			?>
				
				<?php 
				if($i == 1) echo '<div class="row-fluid">';
				?>
				
				<div class="item span4">
					<img src="<?php echo $thumb?>" alt="" />
					<div class="adress">
						<?php echo get_post_meta(get_the_ID(),'endereço',true)?>
					</div>
				</div>
				
				
				<?php 
				if($i == 3) echo '</div>';
				
				if($i == 3) {
					$i = 1;
				} else {
					$i++;
				}
				?>
					
			<?php 
			endwhile;
			?>
		</div>

		<?php pagination_funtion()?>
	
	</div>

</aside>

<?php 
get_footer();
?>